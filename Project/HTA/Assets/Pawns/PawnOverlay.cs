﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PawnOverlay : MonoBehaviour {

	[SerializeField]
	private GameObject addButtonGo;

	public Button.ButtonClickedEvent OnClick { get { return addButtonGo.GetComponent<Button>().onClick; } }

	public void Hide() {
		addButtonGo.SetActive(false);
	}

	public void ShowAddButton() {
		addButtonGo.SetActive(true);
	}

	private void Update() {
		if(Camera.main == null) {
			return;
		}

		transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward, Camera.main.transform.rotation * Vector3.up);
	}

}
