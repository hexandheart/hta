﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour {

	[SerializeField]
	private GameObject tooltip;

	[SerializeField]
	private Text text;

	private RectTransform rect;

	private void Awake() {
		RectTransform overlayRect = GetComponent<RectTransform>();

		overlayRect.offsetMin = Vector2.zero;
		overlayRect.offsetMax = Vector2.zero;

		rect = tooltip.GetComponent<RectTransform>();
	}

	public void Update() {
		PointerEventData eventData = HeroesStandaloneInputModule.GetPointerEventData();
		if(eventData.hovered.Count == 0) {
			tooltip.SetActive(false);
			return;
		}

		TooltipDataProvider dataProvider = null;
		for(int i = eventData.hovered.Count - 1; i >= 0 && dataProvider == null; --i) {
			dataProvider = eventData.hovered[i].GetComponent<TooltipDataProvider>();
			if(dataProvider != null && string.IsNullOrEmpty(dataProvider.GetTooltipData())) {
				dataProvider = null;
			}
		}

		if(dataProvider == null) {
			tooltip.SetActive(false);
			return;
		}

		text.text = dataProvider.GetTooltipData();

		if(!tooltip.activeSelf) {
			tooltip.SetActive(true);
		}
		tooltip.transform.position = Input.mousePosition;
		UpdateAnchor();
	}

	private void UpdateAnchor() {
		Vector2 pivot = Vector2.zero;
		if(tooltip.transform.position.x < Screen.width / 2) {
			pivot.x = -0.05f;
		}
		else {
			pivot.x = 1.05f;
		}

		if(tooltip.transform.position.y < Screen.height / 2) {
			pivot.y = 0f;
		}
		else {
			pivot.y = 1f;
		}

		rect.pivot = pivot;
	}

}