﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DisallowMultipleComponent]
[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(LayoutElement))]
[ExecuteInEditMode]
[SelectionBase]
public class DynamicListItem : MonoBehaviour {

	[SerializeField]
	private int instanceId;

	[SerializeField]
	private float transitionDuration = 0.5f;
	[SerializeField]
	private AnimationCurve transitionCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
	
	[SerializeField]
	private Transform followTarget;

	private int oldFollowId;

	private float moveTime;
	private Vector3 targetPosition;
	private Vector3 lastPosition;

	private int oldSiblingIndex;
	private string oldName;
	
	private LayoutElement layoutElement;

	private bool isInitialized = false;

	private void OnEnable() {
		if(instanceId != GetInstanceID()) {
			followTarget = null;
		}
		instanceId = GetInstanceID();

		oldSiblingIndex = transform.GetSiblingIndex();
		layoutElement = GetComponent<LayoutElement>();
		layoutElement.ignoreLayout = true;

		oldName = name;

		isInitialized = false;
		transform.position = new Vector3(10000f, 1000f);
		StartCoroutine(UpdateCoroutine());
	}

	private void DestroyFollowTarget() {
		if(followTarget != null) {
			DestroyImmediate(followTarget.gameObject);
			followTarget = null;
		}
	}

	private void OnDisable() {
		if(followTarget != null) {
			followTarget.gameObject.SetActive(false);
		}
	}

	private void OnDestroy() {
		DestroyFollowTarget();
	}

	private IEnumerator UpdateCoroutine() {
		yield return new WaitForEndOfFrame();

		if(followTarget == null) {
			if(oldFollowId != 0) {
#if UNITY_EDITOR
				// If we used to have a followTarget, but now we don't, it's because it
				// disappeared when we edited a prefab. Dunno why.
				followTarget = (Transform)UnityEditor.EditorUtility.InstanceIDToObject(oldFollowId);
#endif
			}
			else {
				CreateFollowTarget();
			}
		}
		followTarget.gameObject.SetActive(true);

		LayoutGroup itemLayout = GetComponentInParent<LayoutGroup>();
		if(itemLayout != null) {
			Canvas.ForceUpdateCanvases();
			itemLayout.CalculateLayoutInputHorizontal();
			itemLayout.CalculateLayoutInputVertical();
			itemLayout.SetLayoutHorizontal();
			itemLayout.SetLayoutVertical();
		}

		transform.position = targetPosition = lastPosition = followTarget.position;
		moveTime = 0f;

		isInitialized = true;
	}

	private void CreateFollowTarget() {
		GameObject go = new GameObject(name + " (target)");
		go.transform.SetParent(transform.parent, false);
		RectTransform r = go.AddComponent<RectTransform>();
		r.sizeDelta = GetComponent<RectTransform>().sizeDelta;
		followTarget = go.transform;
		go.transform.SetSiblingIndex(transform.GetSiblingIndex() + 1);
		go.hideFlags = HideFlags.HideAndDontSave;
#if UNITY_EDITOR
		oldFollowId = followTarget.GetInstanceID();
#endif
	}

	private void LateUpdate() {
		if(!isInitialized) {
			return;
		}

		if(followTarget == null) {
			CreateFollowTarget();
		}

		if(followTarget.position != targetPosition) {
			targetPosition = followTarget.position;
			lastPosition = transform.position;
			moveTime = transitionDuration;
		}

		if(moveTime > 0f) {
			if(!Application.isPlaying) {
				transform.position = targetPosition = lastPosition = followTarget.position;
				moveTime = 0f;
			}
			else {
				// Lerp!
				moveTime -= Time.deltaTime;
				if(moveTime <= 0f) {
					transform.position = targetPosition = lastPosition = followTarget.position;
					moveTime = 0f;
				}
				else {
					float percent = 1f - (moveTime / transitionDuration);
					transform.position = Vector3.Lerp(lastPosition, targetPosition, transitionCurve.Evaluate(percent));
				}
			}
		}

		if(name != oldName) {
			oldName = name;
			followTarget.name = name + " (target)";
		}

		if(transform.GetSiblingIndex() != oldSiblingIndex) {
			// Update everybody!
			int nextDliIndex = 0;
			int numItems = transform.parent.childCount;
			for(int i = 0; i < numItems; ++i) {
				Transform child = transform.parent.GetChild(i);
				DynamicListItem dli = child.GetComponent<DynamicListItem>();
				if(dli != null) {
					nextDliIndex = dli.UpdateSiblingIndex(nextDliIndex);
				}
			}
		}
	}

	public int UpdateSiblingIndex(int index) {
		oldSiblingIndex = transform.GetSiblingIndex();
		if(followTarget != null) {
			followTarget.SetSiblingIndex(oldSiblingIndex + 1);
			return oldSiblingIndex + 2;
		}
		else {
			return oldSiblingIndex + 1;
		}
	}

	private void OnRectTransformDimensionsChange() {
		if(followTarget != null) {
			followTarget.GetComponent<RectTransform>().sizeDelta = GetComponent<RectTransform>().sizeDelta;
		}
	}

}
