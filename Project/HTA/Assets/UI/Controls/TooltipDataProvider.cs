﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipDataProvider : MonoBehaviour {

	[SerializeField]
	[Multiline]
	private string tooltipData;

	public string GetTooltipData() {
		return tooltipData;
	}

	public void SetTooltipData(string tooltipData) {
		this.tooltipData = tooltipData;
	}

}