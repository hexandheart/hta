﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPalette : ScriptableObject {

	public Color activityBarColor;
	public Color activityBarEventColor;
	public Color activityBarSuccessColor;
	public Color activityBarFailureColor;

}
