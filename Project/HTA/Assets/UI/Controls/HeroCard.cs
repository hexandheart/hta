﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroCard : MonoBehaviour {

	[SerializeField]
	private LabeledWidget nameLabel;

	[SerializeField]
	private LabeledWidget statsLabel;

	[SerializeField]
	private GameObject selectedFrame;

	[SerializeField]
	private GameObject healthBar;
	[SerializeField]
	private Image healthBarFill;

	[SerializeField]
	private LabeledWidget statusLabel;

	private Hero hero;

	private void Start() {
		if(selectedFrame != null) {
			selectedFrame.SetActive(false);
		}
	}

	public void ShowInfo(Hero hero) {
		this.hero = hero;

		nameLabel.Text = hero.Name;
		if(hero.Job != null) {
			statsLabel.Text = string.Format("<b>Lv {1} {2}</b>\n{0}", hero.Race.Name.ToUpper(), hero.GetLevel(), hero.Job.Name.ToUpper());
		}
		else {
			statsLabel.Text = string.Format("<b>Lv {1}</b>\n{0}", hero.Race.Name.ToUpper(), hero.GetLevel());
		}
	}

	private void Update() {
		if(hero == null) {
			return;
		}
		UpdateStats();
	}

	private void UpdateStats() {
		if(healthBar != null) {
			float percent = hero.Stats.Health / (float)hero.BaseStats.Health;
			healthBarFill.transform.localScale = new Vector3(percent, 1f, 1f);
		}

		if(statusLabel != null) {
			Activity activity = WorldManager.World.GetHeroActivity(hero);
			if(activity != null) {
				statusLabel.Text = activity.Name;
			}
			else {
				Poi poi = WorldManager.World.GetHeroPoi(hero);
				if(poi != null) {
					statusLabel.Text = poi.Definition.Name;
				}
				else {
					statusLabel.Text = "";
				}
			}
		}
	}

	public void OnClicked() {
		hero.TriggerSelected();
	}

	public void OnInfoClicked() {

	}

	public void SetSelected(bool isSelected) {
		if(selectedFrame != null) {
			selectedFrame.SetActive(isSelected);
		}
	}

}
