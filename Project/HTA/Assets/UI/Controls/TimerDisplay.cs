﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerDisplay : MonoBehaviour {

	public float Value {
		get { return fill.fillAmount; }
		set { fill.fillAmount = value; }
	}

	[SerializeField]
	private Image fill;

}
