﻿using UnityEngine;
using System.Collections;
using Events;

public class ActivityOverlay : Menu {

	public override bool IsOverlay { get { return true; } }

	[SerializeField]
	private ActivityList activityList;

	protected override void OnOpen(params object[] args) {
		Activity.OnStarted += activityList.CreateItem;
		activityList.InitializeList();
	}

	protected override void OnClose() {
		Activity.OnStarted -= activityList.CreateItem;
	}

}
