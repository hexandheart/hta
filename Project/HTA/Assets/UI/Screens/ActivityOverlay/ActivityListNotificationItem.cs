﻿using Activities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivityListNotificationItem : MonoBehaviour {

	[SerializeField]
	private LabeledWidget headerLabel;

	[SerializeField]
	private LabeledWidget messageLabel;

	[SerializeField]
	private ActivityListNotification notification;

	private int hideTime;
	private string link;

	public void ShowInfo(string header, string message, string link, int duration) {
		this.link = link;

		headerLabel.Text = header;
		messageLabel.Text = message;
		hideTime = WorldManager.World.CurrentTime + duration;
	}

	private void Update() {
		if(WorldManager.World.CurrentTime >= hideTime) {
			gameObject.SetActive(false);
			Destroy(gameObject);
		}
	}

	public void OnClicked() {
		// Where does the link want us to go?
		if(link.StartsWith("poi:")) {
			string target = link.Substring(4);
			Poi poi = WorldManager.World.GetPoi(target);

			if(poi.Definition.Type == PoiType.Recruitment) {
				MenuManager.OpenMenu<PoiRecruitmentMenu>(poi);
			}
			else if(poi.Definition.Type == PoiType.Questing) {
				MenuManager.OpenMenu<PoiQuestingMenu>(poi);
			}
			else if(poi.Definition.Type == PoiType.Recovery) {
				MenuManager.OpenMenu<PoiRecoveryMenu>(poi);
			}
		}
	}

}
