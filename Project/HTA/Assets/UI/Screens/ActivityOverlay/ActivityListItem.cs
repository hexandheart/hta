﻿using Activities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivityListItem : MonoBehaviour {

	[SerializeField]
	private LabeledWidget headerLabel;

	[SerializeField]
	private Image progressFill;

	[SerializeField]
	private ActivityListNotification notification;

	private Activity activity;

	public void ShowInfo(Activity activity) {
		this.activity = activity;

		activity.OnCompleted += OnActivityCompleted;

		headerLabel.Text = activity.Name;
		UpdateProgress();
	}

	private void OnDisable() {
		if(activity == null) {
			return;
		}
		activity.OnCompleted -= OnActivityCompleted;
	}

	private void OnActivityCompleted(Activity activity) {
		if(this.activity != activity) {
			return;
		}
		gameObject.SetActive(false);
		Destroy(gameObject);
	}

	private void Update() {
		UpdateProgress();
	}

	private void UpdateProgress() {
		float percent = activity.CurrentProgress / (float)activity.TargetProgress;
		progressFill.transform.localScale = new Vector3(percent, 1f, 1f);

		var e = activity.GetCurrentEvent();
		if(e != null) {
			if(e.CanIntervene) {
				notification.Show(e);
			}
			progressFill.color = WorldManager.Palette.activityBarEventColor;
		}
		else {
			notification.Hide();

			ActivityResult result = activity.GetResult();
			if(result == ActivityResult.Success) {
				progressFill.color = WorldManager.Palette.activityBarSuccessColor;
			}
			else if(result == ActivityResult.Failure) {
				progressFill.color = WorldManager.Palette.activityBarFailureColor;
			}
			else {
				progressFill.color = WorldManager.Palette.activityBarColor;
			}
		}
	}

	public void OnClicked() {
		ActivityEvent ae = activity.GetCurrentEvent();
		if(ae != null && ae.CanIntervene) {
			WorldManager.World.SpawnEventScene(ae);
		}
	}

}
