﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivityListNotification : MonoBehaviour {

	[SerializeField]
	private Animator animator;

	private Activities.ActivityEvent activityEvent;

	public void Hide() {
		animator.SetBool("Visible", false);
	}

	public void Show(Activities.ActivityEvent activityEvent) {
		this.activityEvent = activityEvent;
		animator.SetBool("Visible", true);
	}

}
