﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivityList : MonoBehaviour {

	[SerializeField]
	private LayoutGroup itemLayout;

	[SerializeField]
	private ActivityListItem itemPrefab;

	[SerializeField]
	private ActivityListNotificationItem notificationItemPrefab;

	private void OnEnable() {
		Pois.RecoveryPoi.OnHeroRecovered += OnHeroRecovered;
	}

	private void OnDisable() {
		Pois.RecoveryPoi.OnHeroRecovered -= OnHeroRecovered;
	}

	private void OnHeroRecovered(Hero hero, Poi poi) {
		ShowNotification(
			poi.Definition.Name.ToUpper(),
			string.Format("{0} has fully recovered.", hero.Name),
			"poi:" + poi.Definition.ID,
			5);
	}

	public void ShowNotification(string header, string message, string link, int duration) {
		ActivityListNotificationItem item = Instantiate(notificationItemPrefab, transform);
		item.ShowInfo(header, message, link, duration);
	}

	private void ClearList() {
		for(int i = transform.childCount - 1; i >= 0; --i) {
			Transform t = transform.GetChild(i);
			Destroy(t.gameObject);
		}
	}

	public void CreateItem(Activity activity) {
		ActivityListItem item = Instantiate(itemPrefab, transform);
		item.ShowInfo(activity);
	}

	public void InitializeList() {
		ClearList();
		foreach(var activity in WorldManager.World.CurrentActivities) {
			CreateItem(activity);
		}
	}

}
