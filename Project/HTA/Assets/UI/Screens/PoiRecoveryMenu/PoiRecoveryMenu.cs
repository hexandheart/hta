﻿using UnityEngine;
using System.Collections;
using Events;

public class PoiRecoveryMenu : Menu {

	[SerializeField]
	private LabeledWidget title;

	[SerializeField]
	private HeroList heroList;

	[SerializeField]
	private LabeledWidget recoveryInfo;

	private Pois.RecoveryPoi poi;
	private Hero selectedHero;

	private HeroSlot[] heroSlots;

	protected override void OnAwake() {
		heroSlots = GetComponentsInChildren<HeroSlot>(true);
		for(int i = 0; i < heroSlots.Length; ++i) {
			heroSlots[i].SetSlotIndex(i);
		}
	}

	private bool HeroFilter(Hero hero) {
		if(hero == null) {
			return false;
		}

		// Filter out heroes which are assigned.
		if(WorldManager.World.IsHeroBusy(hero)) {
			return false;
		}
		
		// Heroes with full health don't need our services.
		if(hero.Stats.Health >= hero.BaseStats.Health) {
			return false;
		}

		return true;
	}

	protected override void OnOpen(params object[] args) {
		if(args.Length >= 1) {
			poi = args[0] as Pois.RecoveryPoi;
		}

		for(int i = 0; i < heroSlots.Length; ++i) {
			heroSlots[i].gameObject.SetActive(i < poi.RecoveryDefinition.NumSlots);
		}

		selectedHero = null;

		Hero.OnSelected += OnHeroSelected;
		HeroSlot.OnSlotSelected += OnSlotSelected;
		HeroSlot.OnSlotCleared += OnSlotCleared;

		title.Text = poi.Definition.Name.ToUpper();

		heroList.SetFilter(HeroFilter);
		//heroList.RefreshList();
	}

	protected override void OnClose() {
		Hero.OnSelected -= OnHeroSelected;
		HeroSlot.OnSlotSelected -= OnSlotSelected;
		HeroSlot.OnSlotCleared -= OnSlotCleared;
	}

	protected override void OnUpdate() {
		if(!IsOpen) {
			return;
		}

		Refresh();

		if(Input.GetKeyDown(KeyCode.Escape)) {
			OnBackClicked();
		}
	}

	public void OnBackClicked() {
		MenuManager.OpenMenu<WorldMenu>();
	}

	private void Refresh() {
		heroList.UpdateList();
		UpdateDetails();
	}

	private void UpdateDetails() {
		recoveryInfo.Text = string.Format("Recover {0}% health every {1} days.\n\nCOST: <b><q=coin>{2}",
			poi.RecoveryDefinition.Recovery,
			poi.RecoveryDefinition.TimePerRecovery / (float)WorldState.TicksPerDay,
			poi.RecoveryDefinition.GoldCost);

		for(int i = 0; i < heroSlots.Length; ++i) {
			heroSlots[i].SetHero(poi.GetHeroAtIndex(i));
			heroSlots[i].ShowHighlight(selectedHero != null);
		}
	}

	private void OnHeroSelected(Hero hero) {
		selectedHero = hero;
		Refresh();
	}

	private void OnSlotSelected(int slotIndex) {
		if(selectedHero == null) {
			return;
		}

		WorldManager.PushPause();
		if(WorldManager.World.Gold >= poi.RecoveryDefinition.GoldCost) {
			Popup.ShowPopup(
				poi.RecoveryDefinition.Name.ToUpper(),
				string.Format("Rest <b><c=yellow>{0}</c></b> at <b><c=yellow>{1}</c></b> for <b><q=coin>{2}</b>?",
					selectedHero.Name, poi.Definition.Name, poi.RecoveryDefinition.GoldCost),
				Popup.Button.OK | Popup.Button.Cancel,
				(result) => {
					WorldManager.PopPause();

					if(result == Popup.Button.OK) {
						WorldManager.World.Gold -= poi.RecoveryDefinition.GoldCost;
						poi.SetHeroAtIndex(selectedHero, slotIndex, WorldManager.World);

						selectedHero = null;
					}
					Refresh();
				});
		}
		else {
			Popup.ShowPopup(
				poi.RecoveryDefinition.Name.ToUpper(),
				string.Format("Can't afford <b><q=coin>{0}</b> to rest at <b><c=yellow>{1}</c></b>.",
					poi.RecoveryDefinition.GoldCost, poi.Definition.Name),
				Popup.Button.OK,
				(result) => {
					WorldManager.PopPause();
					Refresh();
				});
		}
	}

	private void OnSlotCleared(int slotIndex) {
		poi.SetHeroAtIndex(null, slotIndex, WorldManager.World);

		selectedHero = null;
		Refresh();
	}

}
