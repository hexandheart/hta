﻿using UnityEngine;
using System.Collections;
using Events;

public class PoiRecruitmentMenu : Menu {

	[SerializeField]
	private LabeledWidget title;

	[SerializeField]
	private VisitorList visitorList;

	private Pois.RecruitmentPoi poi;

	protected override void OnOpen(params object[] args) {
		poi = args[0] as Pois.RecruitmentPoi;

		title.Text = poi.Definition.Name.ToUpper();
		visitorList.ShowInfo(poi);
	}

	protected override void OnClose() {
		visitorList.Hide();
	}

	public void OnBackClicked() {
		MenuManager.OpenMenu<WorldMenu>();
	}

	protected override void OnUpdate() {
		if(!IsOpen) {
			return;
		}
		
		if(Input.GetKeyDown(KeyCode.Escape)) {
			OnBackClicked();
		}
	}

}
