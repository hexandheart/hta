﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisitorList : MonoBehaviour {

	[SerializeField]
	private LayoutGroup itemLayout;

	[SerializeField]
	private VisitorListItem itemPrefab;

	private Pois.RecruitmentPoi poi;

	public void ShowInfo(Pois.RecruitmentPoi poi) {
		this.poi = poi;
		poi.OnVisitorArrived += CreateItem;
		InitializeList();
	}

	public void Hide() {
		poi.OnVisitorArrived -= CreateItem;
	}

	private void ClearList() {
		for(int i = transform.childCount - 1; i >= 0; --i) {
			Transform t = transform.GetChild(i);
			Destroy(t.gameObject);
		}
	}

	private void InitializeList() {
		ClearList();
		for(int i = 0; i < poi.Visitors.Count; ++i) {
			CreateItem(poi.Visitors[i]);
		}
	}

	private void CreateItem(Pois.RecruitmentPoi.Visitor visitor) {
		VisitorListItem item = Instantiate(itemPrefab, transform);
		item.ShowInfo(poi, visitor);
	}

	public void OnVisitorHireClicked(Pois.RecruitmentPoi.Visitor visitor) {
		WorldManager.PushPause();

		if(WorldManager.World.Gold >= visitor.hero.GetHireCostGold()) {
			Popup.ShowPopup(
				"HIRE HERO",
				string.Format("Would you like to hire <c=yellow>{0}</c> for <q=coin>{1}?", visitor.hero.Name, visitor.hero.GetHireCostGold()),
				Popup.Button.OK | Popup.Button.Cancel,
				(result) => {
					WorldManager.PopPause();

					if(result == Popup.Button.OK) {
						WorldManager.World.HireHero(visitor.hero);
						poi.RemoveVisitor(visitor);
					}
				});
		}
		else {
			Popup.ShowPopup(
				"HIRE HERO",
				string.Format("You don't have enough <q=coin> to hire <c=yellow>{0}</c> for <q=coin>{1}.", visitor.hero.Name, visitor.hero.GetHireCostGold()),
				Popup.Button.OK,
				(result) => {
					WorldManager.PopPause();
				});
		}
	}

}
