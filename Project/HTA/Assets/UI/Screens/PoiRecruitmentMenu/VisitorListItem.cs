﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisitorListItem : MonoBehaviour {

	[SerializeField]
	private LabeledWidget nameLabel;
	[SerializeField]
	private LabeledWidget infoLabel;

	[SerializeField]
	private HeroCard heroCard;

	[SerializeField]
	private TimerDisplay visitTimer;

	private Pois.RecruitmentPoi poi;
	private Pois.RecruitmentPoi.Visitor visitor;

	public void ShowInfo(Pois.RecruitmentPoi poi, Pois.RecruitmentPoi.Visitor visitor) {
		this.poi = poi;
		this.visitor = visitor;

		heroCard.ShowInfo(visitor.hero);

		poi.OnVisitorDeparted += OnVisitorDeparted;

		nameLabel.Text = visitor.hero.Name.ToUpper();
		infoLabel.Text = string.Format("HIRE: <q=coin>{0}", visitor.hero.GetHireCostGold());
		UpdateVisit();
	}

	private void OnDisable() {
		if(poi != null) {
			poi.OnVisitorDeparted -= OnVisitorDeparted;
		}
	}

	private void Update() {
		UpdateVisit();
	}

	private void UpdateVisit() {
		int duration = visitor.departureTime - visitor.arrivalTime;
		float percent = (WorldManager.World.CurrentTime - visitor.arrivalTime) / (float)duration;
		visitTimer.Value = 1f - percent;
	}

	public void OnHireClicked() {
		GetComponentInParent<VisitorList>().OnVisitorHireClicked(visitor);
	}

	private void OnVisitorDeparted(Pois.RecruitmentPoi.Visitor visitor) {
		if(this.visitor.hero != visitor.hero) {
			return;
		}
		gameObject.SetActive(false);
		Destroy(gameObject);
	}

}
