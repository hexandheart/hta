﻿using UnityEngine;
using System.Collections;
using Events;

public class PoiQuestingMenu : Menu {

	[SerializeField]
	private LabeledWidget title;

	[SerializeField]
	private QuestList questList;

	private Pois.QuestingPoi poi;

	protected override void OnOpen(params object[] args) {
		if(args.Length >= 1) {
			poi = args[0] as Pois.QuestingPoi;
		}

		title.Text = poi.Definition.Name.ToUpper();

		QuestList.OnQuestSelected += OnQuestSelected;
		Quest.OnQuestSpawned += questList.CreateItem;
		questList.ShowInfo(poi);
	}

	public void OnBackClicked() {
		MenuManager.OpenMenu<WorldMenu>();
	}

	protected override void OnClose() {
		QuestList.OnQuestSelected -= OnQuestSelected;
		Quest.OnQuestSpawned -= questList.CreateItem;
	}

	protected override void OnUpdate() {
		if(!IsOpen) {
			return;
		}

		if(Input.GetKeyDown(KeyCode.Escape)) {
			OnBackClicked();
		}
	}

	private void OnQuestSelected(Quest quest) {
		MenuManager.OpenMenu<QuestMenu>(quest);
	}

}
