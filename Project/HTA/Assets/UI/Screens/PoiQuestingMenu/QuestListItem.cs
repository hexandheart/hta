﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestListItem : MonoBehaviour {

	[SerializeField]
	private LabeledWidget nameLabel;

	[SerializeField]
	private LabeledWidget infoLabel;

	[SerializeField]
	private TimerDisplay spawnClock;

	[SerializeField]
	private GameObject activityPanel;

	[SerializeField]
	private LayoutGroup itemLayout;

	[SerializeField]
	private HeroCard[] heroCards;

	[SerializeField]
	private Image progressFill;

	[SerializeField]
	private LabeledWidget etaLabel;

	[SerializeField]
	private Button itemButton;

	private Quest quest;

	public void ShowInfo(Quest quest) {
		this.quest = quest;
		nameLabel.Text = quest.Name.ToUpper();

		infoLabel.Text = string.Format("DURATION: <b>{0}</b>\nREWARD: <b><q=coin>{1}</b>", WorldManager.World.GetTimeSpanLabel(quest.GetExpectedDuration()), quest.RewardGold);

		UpdateInfo();

		quest.OnQuestExpired += OnQuestExpired;
	}

	private void OnDisable() {
		if(quest == null) {
			return;
		}
		quest.OnQuestExpired -= OnQuestExpired;
	}

	private void Update() {
		UpdateInfo();
	}

	private void UpdateInfo() {
		if(quest == null) {
			return;
		}

		if(quest.IsActive()) {
			spawnClock.gameObject.SetActive(false);
			itemButton.interactable = false;
			UpdateActivityPanel();
		}
		else if(quest.Result == ActivityResult.Incomplete) {
			activityPanel.SetActive(false);
			itemButton.interactable = true;
			UpdateSpawnTimer();
		}
		else {
			spawnClock.gameObject.SetActive(false);
			activityPanel.SetActive(false);
		}
	}

	private void UpdateSpawnTimer() {
		if(quest.DespawnTime < quest.SpawnTime) {
			// Doesn't expire!
			spawnClock.gameObject.SetActive(false);
			return;
		}
		spawnClock.gameObject.SetActive(true);
		int duration = quest.DespawnTime - quest.SpawnTime;
		float percent = (WorldManager.World.CurrentTime - quest.SpawnTime) / (float)duration;
		spawnClock.Value = 1f - percent;
	}

	private void UpdateActivityPanel() {
		activityPanel.SetActive(true);

		int index = 0;
		for(; index < quest.Activity.GetNumHeroes() && index < heroCards.Length; ++index) {
			Hero hero = quest.Activity.GetHero(index);
			if(hero != null) {
				heroCards[index].ShowInfo(hero);
				heroCards[index].gameObject.SetActive(true);
			}
			else {
				heroCards[index].gameObject.SetActive(false);
			}
		}
		for(; index < heroCards.Length; ++index) {
			heroCards[index].gameObject.SetActive(false);
		}

		Canvas.ForceUpdateCanvases();
		itemLayout.CalculateLayoutInputHorizontal();
		itemLayout.CalculateLayoutInputVertical();
		itemLayout.SetLayoutHorizontal();
		itemLayout.SetLayoutVertical();

		progressFill.transform.localScale = new Vector3(quest.Activity.CurrentProgress / (float)quest.Activity.TargetProgress, 1f, 1f);
		//etaLabel.Text = string.Format("ETA <b><c=yellow>{0}</c></b>", WorldManager.World.GetTimeSpanLabel(quest.Activity.TargetProgress - quest.Activity.CurrentProgress));
		Activities.ActivityEvent e = quest.Activity.GetCurrentEvent();
		if(e != null) {
			etaLabel.Text = e.Type.ToString();
			progressFill.color = WorldManager.Palette.activityBarEventColor;
		}
		else {
			etaLabel.Text = quest.Activity.GetResult().ToString();

			ActivityResult result = quest.Activity.GetResult();
			if(result == ActivityResult.Success) {
				progressFill.color = WorldManager.Palette.activityBarSuccessColor;
			}
			else if(result == ActivityResult.Failure) {
				progressFill.color = WorldManager.Palette.activityBarFailureColor;
			}
			else {
				progressFill.color = WorldManager.Palette.activityBarColor;
			}
		}
	}

	public void OnClicked() {
		if(!quest.IsActive()) {
			GetComponentInParent<QuestList>().OnQuestClicked(quest);
		}
	}

	private void OnQuestExpired(Quest quest) {
		if(this.quest != quest) {
			return;
		}
		gameObject.SetActive(false);
		Destroy(gameObject);
	}

}
