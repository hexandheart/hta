﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestList : MonoBehaviour {
	
	public static event Quest.QuestHandler OnQuestSelected;

	[SerializeField]
	private LayoutGroup itemLayout;

	[SerializeField]
	private QuestListItem itemPrefab;

	private Pois.QuestingPoi poi;
	
	private void ClearList() {
		for(int i = transform.childCount - 1; i >= 0; --i) {
			Transform t = transform.GetChild(i);
			Destroy(t.gameObject);
		}
	}

	public void CreateItem(Quest quest) {
		if(!poi.Quests.Contains(quest)) {
			return;
		}
		QuestListItem item = Instantiate(itemPrefab, transform);
		item.ShowInfo(quest);
	}

	private void InitializeList() {
		ClearList();
		foreach(var quest in poi.Quests) {
			CreateItem(quest);
		}
	}

	public void ShowInfo(Pois.QuestingPoi poi) {
		this.poi = poi;

		InitializeList();
	}

	public void OnQuestClicked(Quest quest) {
		OnQuestSelected?.Invoke(quest);
	}

}
