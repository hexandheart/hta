﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PoiList : MonoBehaviour {
	
	public static event Poi.PoiHandler OnPoiSelected;

	[SerializeField]
	private LabeledWidget label;

	[SerializeField]
	private LayoutGroup itemLayout;

	[SerializeField]
	private PoiListItem itemPrefab;

	private Region currentRegion = null;

	public void ShowRegion(Region region) {
		currentRegion = region;
		if(region == null) {
			gameObject.SetActive(false);
		}
		else {
			if(currentRegion.Definition.IsHome) {
				label.Text = "<q=home-icon> " + currentRegion.Definition.Name.ToUpper();
			}
			else {
				label.Text = currentRegion.Definition.Name.ToUpper();
			}

			ClearList();
			foreach(var poi in region.Pois) {
				CreateItem(poi);
			}

			gameObject.SetActive(true);
		}
	}

	private void ClearList() {
		for(int i = itemLayout.transform.childCount - 1; i >= 0; --i) {
			Transform t = itemLayout.transform.GetChild(i);
			Destroy(t.gameObject);
		}
	}

	public PoiListItem CreateItem(Poi poi) {
		PoiListItem item = Instantiate(itemPrefab, itemLayout.transform);
		item.ShowInfo(poi);
		return item;
	}

	public void OnPoiClicked(Poi poi) {
		OnPoiSelected?.Invoke(poi);
	}

}
