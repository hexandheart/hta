﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionListItem : MonoBehaviour {

	[SerializeField]
	private LabeledWidget label;

	[SerializeField]
	private GameObject highlight;

	private Region region;

	public void ShowInfo(Region region) {
		this.region = region;
		if(region.Definition.IsHome) {
			label.Text = "<q=home-icon> " + region.Definition.Name.ToUpper();
		}
		else {
			label.Text = region.Definition.Name.ToUpper();
		}
	}

	public void SetHighlight(bool isHighlighted) {
		highlight.SetActive(isHighlighted);
	}

	public void OnClicked() {
		GetComponentInParent<RegionList>().OnRegionClicked(region);
	}

}
