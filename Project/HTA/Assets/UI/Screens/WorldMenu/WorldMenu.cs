﻿using UnityEngine;
using System.Collections;
using Events;

public class WorldMenu : Menu {

	[SerializeField]
	private RegionList regionList;

	[SerializeField]
	private PoiList poiList;

	private Region selectedRegion = null;

	protected override void OnAwake() {
		GameStateManager.OnGameStateChanged += OnGameStateChanged;
	}

	private void OnGameStateChanged(string state, TransitionState transition) {
		if(state == "WorldHub" && transition == TransitionState.Entering) {
			// If there's an active event scene, we have to go to that.
			if(WorldManager.World.ActiveEventScene != null) {
				WorldManager.World.ActiveEventScene.LoadScene();
			}
			else {
				MenuManager.OpenMenu<WorldMenu>();
			}
		}
	}

	protected override void OnOpen(params object[] args) {
		RegionList.OnRegionSelected += OnRegionSelected;
		PoiList.OnPoiSelected += OnPoiSelected;

		regionList.Initialize();
		if(selectedRegion == null) {
			selectedRegion = WorldManager.World.GetRegion("home");
		}
		OnRegionSelected(selectedRegion);

		MenuManager.OpenOverlay<MenuOverlay>();
		MenuManager.OpenOverlay<ActivityOverlay>();
	}

	protected override void OnClose() {
		RegionList.OnRegionSelected -= OnRegionSelected;
		PoiList.OnPoiSelected -= OnPoiSelected;
	}

	private void OnRegionSelected(Region region) {
		selectedRegion = region;
		regionList.SetSelectedRegion(region);
		poiList.ShowRegion(region);
	}

	private void OnPoiSelected(Poi poi) {
		if(poi.Definition.Type == PoiType.Recruitment) {
			MenuManager.OpenMenu<PoiRecruitmentMenu>(poi);
		}
		else if(poi.Definition.Type == PoiType.Questing) {
			MenuManager.OpenMenu<PoiQuestingMenu>(poi);
		}
		else if(poi.Definition.Type == PoiType.Recovery) {
			MenuManager.OpenMenu<PoiRecoveryMenu>(poi);
		}
	}

}
