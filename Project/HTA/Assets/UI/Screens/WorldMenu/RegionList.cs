﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegionList : MonoBehaviour {
	
	public static event Region.RegionHandler OnRegionSelected;

	[SerializeField]
	private LayoutGroup itemLayout;

	[SerializeField]
	private RegionListItem itemPrefab;

	private Region selectedRegion = null;
	private Dictionary<Region, RegionListItem> listItems;

	private void ClearList() {
		for(int i = itemLayout.transform.childCount - 1; i >= 0; --i) {
			Transform t = itemLayout.transform.GetChild(i);
			Destroy(t.gameObject);
		}
		listItems = new Dictionary<Region, RegionListItem>();
	}

	public void CreateItem(Region region) {
		RegionListItem item = Instantiate(itemPrefab, itemLayout.transform);
		item.ShowInfo(region);
		listItems[region] = item;
	}

	public void Initialize() {
		ClearList();
		foreach(var region in WorldManager.World.Regions) {
			CreateItem(region);
		}
	}

	public void SetSelectedRegion(Region region) {
		if(selectedRegion != null && listItems.ContainsKey(region)) {
			listItems[selectedRegion].SetHighlight(false);
		}
		selectedRegion = region;
		if(selectedRegion != null && listItems.ContainsKey(region)) {
			listItems[selectedRegion].SetHighlight(true);
		}
	}

	public void OnRegionClicked(Region region) {
		OnRegionSelected?.Invoke(region);
	}

}
