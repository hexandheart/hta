﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoiListItem : MonoBehaviour {

	[SerializeField]
	private LabeledWidget label;

	private Poi poi;

	public void ShowInfo(Poi poi) {
		this.poi = poi;
		label.Text = poi.Definition.Name.ToUpper();
	}

	public void OnClicked() {
		GetComponentInParent<PoiList>().OnPoiClicked(poi);
	}

}
