﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyDisplay : MonoBehaviour {

	private static readonly string GoldFormat = "<q=coin>{0}";

	[SerializeField]
	private LabeledWidget goldLabel;

	private void Update() {
		if(WorldManager.World == null) {
			return;
		}

		goldLabel.Text = string.Format(GoldFormat, WorldManager.World.Gold);
	}

}
