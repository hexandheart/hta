﻿using UnityEngine;
using System.Collections;
using Events;

public class MenuOverlay : Menu {

	public override bool IsOverlay { get { return true; } }

	protected override void OnOpen(params object[] args) {
	}

	public void OnWorldClicked() {
		MenuManager.OpenMenu<WorldMenu>();
	}

	public void OnCollectionClicked() {
		MenuManager.OpenMenu<CollectionMenu>();
	}

}
