﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimSpeedButton : MonoBehaviour {

	private Animator animator;

	private void Start() {
		animator = GetComponent<Animator>();
	}

	public void OnClick() {
		switch(WorldManager.SimSpeed) {
		case SimSpeed.Paused:
			WorldManager.SimSpeed = SimSpeed.Normal;
			break;
		case SimSpeed.Normal:
			WorldManager.SimSpeed = SimSpeed.Paused;
			break;
		}
		animator.SetInteger("SimSpeed", (int)WorldManager.SimSpeed);
	}

	public void Update() {
		if(WorldManager.World == null) {
			return;
		}

		if(Input.GetKeyDown(KeyCode.Space)) {
			OnClick();
		}

		if((int)WorldManager.SimSpeed != animator.GetInteger("SimSpeed")) {
			animator.SetInteger("SimSpeed", (int)WorldManager.SimSpeed);
		}
	}

}
