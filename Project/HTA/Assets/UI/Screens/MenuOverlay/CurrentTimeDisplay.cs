﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentTimeDisplay : MonoBehaviour {

	[SerializeField]
	private LabeledWidget dayLabel;

	[SerializeField]
	private LabeledWidget yearLabel;

	private void Update() {
		if(WorldManager.World == null) {
			return;
		}

		dayLabel.Text = WorldManager.World.GetDay().ToString();
		yearLabel.Text = WorldManager.World.GetYear().ToString();
	}

}
