﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldListItem : MonoBehaviour {

	[SerializeField]
	private LabeledWidget label;

	[SerializeField]
	private TooltipDataProvider tooltipData;

	private WorldHeader world;

	public void ShowInfo(WorldHeader world) {
		this.world = world;
		label.Text = string.Format("{0}", world.WorldName);

		tooltipData.SetTooltipData(string.Format("{0} : {1}", world.WorldName, world.WorldSalt));
	}

	public void OnClicked() {
		GetComponentInParent<WorldList>().OnWorldSelected(world);
	}

	public void OnDeleteClicked() {
		GetComponentInParent<WorldList>().OnWorldDeleteSelected(world);
	}

}
