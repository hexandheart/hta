﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldList : MonoBehaviour {

	[SerializeField]
	private LayoutGroup itemLayout;

	[SerializeField]
	private GameObject createWorldButton;

	private WorldListItem[] items;

	private bool isWorldLoaded = false;

	private void Awake() {
		items = GetComponentsInChildren<WorldListItem>(true);
		for(int i = 0; i < items.Length; ++i) {
			items[i].gameObject.SetActive(false);
		}

		WorldManager.OnWorldLoaded += OnWorldLoaded;
	}

	private void OnDestroy() {
		WorldManager.OnWorldLoaded -= OnWorldLoaded;
	}

	public void RefreshList() {
		int index = 0;

		List<WorldHeader> headers = WorldManager.GetWorldList();
		for(; index < headers.Count && index < items.Length; ++index) {
			items[index].ShowInfo(headers[index]);
			items[index].gameObject.SetActive(true);
		}

		for(; index < items.Length; ++index) {
			items[index].gameObject.SetActive(false);
		}
		
		Canvas.ForceUpdateCanvases();
		itemLayout.CalculateLayoutInputHorizontal();
		itemLayout.CalculateLayoutInputVertical();
		itemLayout.SetLayoutHorizontal();
		itemLayout.SetLayoutVertical();
	}

	public void OnCreateClicked() {
		Popup.ShowPopup(
			"CREATE WORLD", // Loc
			"What would you like to name the new world?", // Loc
			Popup.Button.OK | Popup.Button.Cancel | Popup.Button.Input,
			"New World", // Loc
			"Enter world name...", //Loc
			result => {
				if(result != Popup.Button.OK ||
					string.IsNullOrEmpty(Popup.TextEntry)) {
					return;
				}
				GameStateManager.RequestState("WorldHub");
				GameStateManager.RegisterTransitionCoroutine(WaitForWorldLoad());
				WorldManager.CreateNewWorld(Popup.TextEntry);
			});
	}

	public void OnWorldSelected(WorldHeader world) {
		GameStateManager.RequestState("WorldHub");
		GameStateManager.RegisterTransitionCoroutine(WaitForWorldLoad());
		WorldManager.LoadWorld(world);
	}

	public void OnWorldDeleteSelected(WorldHeader world) {
		Popup.ShowPopup(
			"DELETE WORLD", // Loc
			string.Format("Are you sure you want to delete the world '{0} ({1})'?", world.WorldName, world.WorldSalt), // Loc
			Popup.Button.OK | Popup.Button.Cancel,
			result => {
				if(result != Popup.Button.OK) {
					return;
				}
				WorldManager.DeleteWorld(world);
				RefreshList();
			});
	}

	private void OnWorldLoaded() {
		isWorldLoaded = true;
	}

	private IEnumerator WaitForWorldLoad() {
		isWorldLoaded = false;
		while(!isWorldLoaded) {
			yield return null;
		}
	}

}
