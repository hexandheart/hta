﻿using UnityEngine;
using System.Collections;
using Events;

public class MainMenu : Menu {

	[SerializeField]
	private WorldList worldList;

	protected override void OnAwake() {
		GameStateManager.OnGameStateChanged += OnGameStateChaned;
	}

	protected override void OnOpen(params object[] args) {
		worldList.RefreshList();
	}

	private void OnGameStateChaned(string state, TransitionState transition) {
		if(state == "MainMenu" && transition == TransitionState.Entering) {
			MenuManager.OpenMenu<MainMenu>();
		}
	}

}
