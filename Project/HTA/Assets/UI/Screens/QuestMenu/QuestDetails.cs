﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestDetails : MonoBehaviour {

	[SerializeField]
	private LayoutGroup itemLayout;

	[SerializeField]
	private LabeledWidget infoLabel;

	private Quest quest;
	private HeroSlot[] heroSlots;

	private void Start() {
		heroSlots = GetComponentsInChildren<HeroSlot>(true);
	}

	public void ShowInfo(Quest quest) {
		this.quest = quest;
		infoLabel.Text = string.Format("{0}\n\nDURATION: <b>{1}</b>\nREWARD: <b><q=coin>{2}</b>",
			quest.Info,
			WorldManager.World.GetTimeSpanLabel(quest.GetExpectedDuration()),
			quest.RewardGold);

		UpdateSlots();
	}

	public void ShowHighlight(bool isHighlighted) {
		for(int i = 0; i < heroSlots.Length; ++i) {
			heroSlots[i].ShowHighlight(isHighlighted);
			heroSlots[i].SetSlotIndex(i);
		}
	}

	private void UpdateSlots() {
		heroSlots = GetComponentsInChildren<HeroSlot>(true);

		int index = 0;
		for(; index < quest.GetNumSlots() && index < heroSlots.Length; ++index) {
			heroSlots[index].gameObject.SetActive(true);
			heroSlots[index].SetHero(quest.GetHeroInSlot(index));
		}
		for(; index < heroSlots.Length; ++index) {
			heroSlots[index].gameObject.SetActive(false);
		}

		Canvas.ForceUpdateCanvases();
		itemLayout.CalculateLayoutInputHorizontal();
		itemLayout.CalculateLayoutInputVertical();
		itemLayout.SetLayoutHorizontal();
		itemLayout.SetLayoutVertical();
	}

}
