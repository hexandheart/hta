﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroSlot : MonoBehaviour {

	public delegate void SlotSelectedDelegate(int slotIndex);
	public static event SlotSelectedDelegate OnSlotSelected;

	public delegate void SlotClearedDelegate(int slotIndex);
	public static event SlotClearedDelegate OnSlotCleared;

	public Hero Hero { get { return hero; } }

	[SerializeField]
	private HeroCard heroCard;

	[SerializeField]
	private GameObject removeButton;

	[SerializeField]
	private GameObject highlight;

	private int slotIndex;
	private Hero hero;

	public void SetSlotIndex(int slotIndex) {
		this.slotIndex = slotIndex;
	}

	public void ShowHighlight(bool show) {
		highlight.SetActive(show);
	}

	public void SetHero(Hero hero) {
		this.hero = hero;

		if(hero != null) {
			removeButton.SetActive(true);
			heroCard.gameObject.SetActive(true);
			heroCard.ShowInfo(hero);
		}
		else {
			removeButton.SetActive(false);
			heroCard.gameObject.SetActive(false);
		}
	}

	public void OnClicked() {
		OnSlotSelected?.Invoke(slotIndex);
	}

	public void OnRemoveClicked() {
		OnSlotCleared?.Invoke(slotIndex);
	}

}
