﻿using UnityEngine;
using System.Collections;
using Events;
using UnityEngine.UI;

public class QuestMenu : Menu {

	[SerializeField]
	private LabeledWidget headerLabel;

	[SerializeField]
	private HeroList heroList;

	[SerializeField]
	private QuestDetails questDetails;

	[SerializeField]
	private Button startButton;

	private Quest quest;
	private Quest tempQuest;

	private bool HeroFilter(Hero hero) {
		if(hero == null) {
			return false;
		}

		// Filter out heroes which are assigned.
		if(WorldManager.World.IsHeroBusy(hero)) {
			return false;
		}
		// Filter out heroes which are assigned locally too.
		for(int i = 0; i < tempQuest.GetNumSlots(); ++i) {
			if(tempQuest.GetHeroInSlot(i) == hero) {
				return false;
			}
		}
		return true;
	}

	protected override void OnOpen(params object[] args) {
		quest = args[0] as Quest;
		tempQuest = new Quest(quest);

		WorldManager.PushPause();

		Hero.OnSelected += OnHeroSelected;
		HeroSlot.OnSlotSelected += OnSlotSelected;
		HeroSlot.OnSlotCleared += OnSlotCleared;

		headerLabel.Text = string.Format("PREPARE QUEST: <i>{0}</i>", quest.Name.ToUpper());

		heroList.SetFilter(HeroFilter);
		heroList.InitializeList(null);
		Refresh();
	}

	protected override void OnClose() {
		Hero.OnSelected -= OnHeroSelected;
		HeroSlot.OnSlotSelected -= OnSlotSelected;
		HeroSlot.OnSlotCleared -= OnSlotCleared;

		WorldManager.PopPause();
	}

	private void Refresh() {
		UpdateDetails();
		questDetails.ShowHighlight(heroList.SelectedHero != null);
	}

	private void UpdateDetails() {
		questDetails.ShowInfo(tempQuest);

		// Is the quest good to go?
		startButton.interactable = tempQuest.CanStartQuest();
	}

	public void OnBackClicked() {
		// Temp quest get discarded.
		tempQuest = null;
		MenuManager.OpenMenu<PoiQuestingMenu>();
	}

	public void OnStartClicked() {
		// Assign heroes.
		for(int i = 0; i < quest.GetNumSlots(); ++i) {
			quest.SetHeroInSlot(tempQuest.GetHeroInSlot(i), i);
		}
		WorldManager.World.StartQuest(quest);
		MenuManager.OpenMenu<PoiQuestingMenu>();
	}

	private void OnHeroSelected(Hero hero) {
		heroList.SetSelectedHero(hero);
		heroList.UpdateList();
		Refresh();
	}

	private void OnSlotSelected(int slotIndex) {
		if(heroList.SelectedHero == null) {
			return;
		}

		tempQuest.SetHeroInSlot(heroList.SelectedHero, slotIndex);
		heroList.SetSelectedHero(null);
		heroList.UpdateList();
		Refresh();
	}

	private void OnSlotCleared(int slotIndex) {
		tempQuest.SetHeroInSlot(null, slotIndex);
		heroList.SetSelectedHero(null);
		heroList.UpdateList();
		Refresh();
	}

}
