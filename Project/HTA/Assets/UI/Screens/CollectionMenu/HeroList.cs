﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroList : MonoBehaviour {

	[SerializeField]
	private LayoutGroup itemLayout;

	[SerializeField]
	private HeroListItem itemPrefab;

	private Func<Hero, bool> filter;

	private Hero selectedHero = null;
	private Dictionary<Hero, HeroListItem> listItems;

	[SerializeField]
	private float updateListTime = 1f;
	private float updateListTimer = 0f;

	public Hero SelectedHero {
		get { return selectedHero; }
	}

	public void SetFilter(Func<Hero, bool> filter) {
		this.filter = filter;
	}

	private void Start() {
		ClearList();
		updateListTimer = 0f;
	}

	private void Update() {
		if(WorldManager.World == null) {
			return;
		}
		updateListTimer -= Time.deltaTime;
		if(updateListTimer <= 0f) {
			updateListTimer = updateListTime;
			UpdateList();
		}
	}

	private void ClearList() {
		for(int i = itemLayout.transform.childCount - 1; i >= 0; --i) {
			Transform t = itemLayout.transform.GetChild(i);
			Destroy(t.gameObject);
		}
		listItems = new Dictionary<Hero, HeroListItem>();
	}

	public HeroListItem CreateItem(Hero hero) {
		HeroListItem item = Instantiate(itemPrefab, itemLayout.transform);
		item.ShowInfo(hero);
		listItems[hero] = item;
		return item;
	}

	public void InitializeList(Hero selectedHero = null) {
		ClearList();
		UpdateList();
		SetSelectedHero(selectedHero);
	}

	public void SetSelectedHero(Hero selectedHero) {
		if(this.selectedHero != null && listItems.ContainsKey(this.selectedHero)) {
			listItems[this.selectedHero].SetSelected(false);
		}
		this.selectedHero = selectedHero;
		if(this.selectedHero != null && listItems.ContainsKey(this.selectedHero)) {
			listItems[this.selectedHero].SetSelected(true);
		}
	}

	public void UpdateList() {
		if(WorldManager.World.Heroes == null) {
			return;
		}

		// Do we have new heroes to show?
		int listIndex = 0;
		foreach(var hero in WorldManager.World.Heroes) {
			// Filter out heroes that don't pass the test.
			if(filter != null && !filter(hero)) {
				// Hide the hero if its item exists!
				if(listItems.ContainsKey(hero)) {
					listItems[hero].gameObject.SetActive(false);
				}
				continue;
			}

			// If the hero's item exists, show it!
			if(listItems.ContainsKey(hero)) {
				listItems[hero].gameObject.SetActive(true);
			}
			// Otherwise create it!
			else {
				CreateItem(hero);
			}

			// Make sure it's in the right position...?
			listItems[hero].transform.SetSiblingIndex(listIndex);
			++listIndex;
		}

		// The only way that there would be a hero in this list that isn't in
		// the world heroes list is if the hero was deleted...
	}

	private void OnHeroClicked(Hero hero) {
		hero.TriggerSelected();
	}

}
