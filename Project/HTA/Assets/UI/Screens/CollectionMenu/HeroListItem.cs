﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroListItem : MonoBehaviour {

	[SerializeField]
	private HeroCard heroCard;

	public void ShowInfo(Hero hero, bool isSelected = false) {
		gameObject.SetActive(true);
		heroCard.ShowInfo(hero);
		heroCard.SetSelected(isSelected);
	}

	public void SetSelected(bool isSelected) {
		heroCard.SetSelected(isSelected);
	}

}
