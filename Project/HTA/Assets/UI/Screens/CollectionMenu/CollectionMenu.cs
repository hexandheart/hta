﻿using UnityEngine;
using System.Collections;
using Events;

public class CollectionMenu : Menu {

	[SerializeField]
	private HeroList heroList;

	protected override void OnOpen(params object[] args) {
		Hero.OnSelected += OnHeroSelected;
	}

	protected override void OnClose() {
		Hero.OnSelected -= OnHeroSelected;
	}

	private void OnHeroSelected(Hero hero) {
		string message = string.Format("Hero {0} skills:\n", hero.Name);
		foreach(var skill in hero.Skills) {
			int curXp = skill.GetCurrentLevelXP();
			int reqXp = skill.GetCurrentLevelRequiredXP();
			message += string.Format("- Skill {0}: level {1} ({5}: {2}/{3} ({4}%))\n",
				skill.Definition.Name,
				skill.GetLevel(),
				curXp,
				reqXp,
				reqXp == 0 ? "MAX" : (curXp * 100 / reqXp).ToString(),
				skill.XP);
		}
		message += "Spells:\n";
		foreach(var spell in hero.GetAvailableSpells(WorldManager.World)) {
			message += string.Format("- {0}\n", spell.Name);
		}
		Debug.Log(message);
	}

}
