﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace UnityEngine.EventSystems {
	[AddComponentMenu("Event/Heroes Standalone Input Module")]
	public class HeroesStandaloneInputModule : StandaloneInputModule {

		public static PointerEventData GetPointerEventData(int pointerId = -1) {
			PointerEventData eventData;
			instance.GetPointerData(pointerId, out eventData, true);
			return eventData;
		}

		protected override void Awake() {
			base.Awake();
			instance = this;
		}

		private static HeroesStandaloneInputModule instance;

	}
}