﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemTest : MonoBehaviour {

	[SerializeField]
	private Image image;

	[SerializeField]
	private float minTime;

	[SerializeField]
	private float maxTime;

	private float curTime = 0f;
	private float endTime = 0f;

	private void OnEnable() {
		curTime = 0f;
		endTime = Random.value * (maxTime - minTime) + minTime;
		image.fillAmount = 0f;
	}

	private void Update() {
		curTime += Time.deltaTime;
		float percent = curTime / endTime;
		if(percent >= 1f) {
			percent = 1f;
			gameObject.SetActive(false);
			Destroy(gameObject);
			return;
		}

		image.fillAmount = percent;
	}

}
