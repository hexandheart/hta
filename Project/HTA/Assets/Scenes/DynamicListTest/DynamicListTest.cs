﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicListTest : MonoBehaviour {

	[SerializeField]
	private Transform itemPrefab;

	[SerializeField]
	private Transform itemList;

	public void OnAddItemClicked() {
		Transform t = Instantiate(itemPrefab, itemList, false);
		t.name = System.Guid.NewGuid().ToString().Substring(0, 8);
	}

}
