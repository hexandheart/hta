﻿using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill  {

	public SkillDefinition Definition { get; private set; }
	public XPTable XPTable { get; private set; }
	public int XP { get; private set; }

	public int GetLevel() {
		return XPTable.GetLevelForTotalXP(XP);
	}

	public int GetCurrentLevelXP() {
		return XP - XPTable.GetTotalXPForLevel(GetLevel());
	}

	public int GetCurrentLevelRequiredXP() {
		int level = GetLevel();
		if(level == XPTable.GetMaxLevel()) {
			return 0;
		}
		return XPTable.GetTotalXPForLevel(level + 1) - XPTable.GetTotalXPForLevel(level);
	}

	public void GetAvailableSpells(List<SpellDefinition> spells, WorldState world) {
		int level = GetLevel();
		for(int i = 0; i < Definition.Spells.Length; ++i) {
			if(level < Definition.Spells[i].Level) {
				continue;
			}

			SpellDefinition spellDefinition = world.GetSpellDefinition(Definition.Spells[i].SpellID);
			if(spells.Contains(spellDefinition)) {
				continue;
			}

			spells.Add(spellDefinition);
		}
	}

	/************************************************************************/
	#region Construction

	private Skill(
			SkillDefinition definition,
			XPTable xpTable,
			int xp) {
		Definition = definition;
		XPTable = xpTable;
		XP = xp;
	}

	public static void GenerateHireSkills(MantleDefinition race, MantleDefinition job, List<Skill> skills, WorldState world) {
		GenerateHireSkillsForMantle(race, skills, world);
		GenerateHireSkillsForMantle(job, skills, world);
	}

	private static void GenerateHireSkillsForMantle(MantleDefinition mantle, List<Skill> skills, WorldState world) {
		if(mantle == null) {
			return;
		}
		for(int i = 0; i < mantle.HireSkills.Length; ++i) {
			MantleDefinition.MantleHireSkill hireSkill = mantle.HireSkills[i];
			if(skills.Find(s => s.Definition.ID == hireSkill.ID) != null) {
				continue;
			}

			SkillDefinition definition = world.GetSkillDefinition(hireSkill.ID);
			XPTable xpTable = world.GetXPTable(definition.XPTableID);

			int xp = Random.Range(hireSkill.XPMin, hireSkill.XPMax + 1);
			skills.Add(new Skill(definition, xpTable, xp));
		}
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static Skill FromJson(JsonData root, WorldState world) {
		string id = (string)root["id"];
		SkillDefinition definition = world.GetSkillDefinition(id);
		XPTable xpTable = world.GetXPTable(definition.XPTableID);

		int xp = 0;
		if(root.Keys.Contains("xp")) {
			xp = (int)root["xp"];
		}

		return new Skill(definition, xpTable, xp);
	}

	public JsonData ToJson() {
		return new JsonData() {
			["id"] = Definition.ID,
			["xp"] = XP,
		};
	}

	#endregion

}
