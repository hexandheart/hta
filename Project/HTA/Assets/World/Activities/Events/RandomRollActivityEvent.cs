﻿using System;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using UnityEngine;

namespace Activities.Events {

	public class RandomRollActivityEvent : ActivityEvent {

		public override ActivityEventType Type { get { return ActivityEventType.RandomRoll; } }
		public override bool CanIntervene { get { return false; } }

		public int Chance { get; private set; }

		public override void AutoResolve(WorldState world) {
			Result = ActivityResult.Failure;
			if(UnityEngine.Random.Range(0, 100) < Chance) {
				Result = ActivityResult.Success;
			}
		}

		/************************************************************************/
		#region Construction

		/// <summary>
		/// Used for creating a new event (see QuestActivity.Create()).
		/// </summary>
		/// <param name="eventDefinition"></param>
		/// <param name="triggerProgress"></param>
		/// <param name="world"></param>
		public RandomRollActivityEvent(ActivityEventDefinition eventDefinition, int triggerProgress, WorldState world) :
				base(eventDefinition.EventIndex, triggerProgress, eventDefinition.TriggerDuration, ActivityResult.Incomplete) {
			Chance = 100;
			if(eventDefinition.Data != null && eventDefinition.Data.Keys.Contains("chance")) {
				Chance = (int)eventDefinition.Data["chance"];
			}
		}

		/// <summary>
		/// Used for loading an event.
		/// </summary>
		/// <param name="triggerTime"></param>
		/// <param name="triggerDuration"></param>
		/// <param name="result"></param>
		/// <param name="root"></param>
		/// <param name="world"></param>
		public RandomRollActivityEvent(int eventIndex, int triggerTime, int triggerDuration, ActivityResult result, JsonData root, WorldState world) :
				base(eventIndex, triggerTime, triggerDuration, result) {
			Chance = 100;
			if(root.Keys.Contains("chance")) {
				Chance = (int)root["chance"];
			}
		}

		#endregion

		/************************************************************************/
		#region Serialization

		protected override void FillSaveData(JsonData root) {
			root["chance"] = Chance;
		}

		#endregion

	}

}
