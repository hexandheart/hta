﻿using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Activities.Events {

	public abstract class EventScene {

		public abstract ActivityEventType Type { get; }
		public abstract string SceneName { get; }
		
		public ActivityEvent ActivityEvent { get; private set; }

		public void LoadScene() {
			GameStateManager.RequestState(SceneName);
		}

		protected void CompleteScene(string nextState, WorldState world) {
			world.ClearEventScene();
			GameStateManager.RequestState(nextState);
		}

		public void OnGameStateChanged(string state, TransitionState transition) {
			if(state == SceneName) {
				if(transition == TransitionState.Entering) {
					MenuManager.OpenMenu<EmptyMenu>();
					isLoaded = false;
					AsyncOperation op = SceneManager.LoadSceneAsync(SceneName, LoadSceneMode.Single);
					if(op != null) {
						GameStateManager.RegisterTransitionCoroutine(WaitForOperation(op));
					}
				}
				else if(transition == TransitionState.Exiting) {
					GameStateManager.OnGameStateChanged -= OnGameStateChanged;
					AsyncOperation op = SceneManager.LoadSceneAsync("Empty");
					if(op != null) {
						GameStateManager.RegisterTransitionCoroutine(WaitForOperation(op));
					}
				}
			}
		}

		/************************************************************************/
		#region Construction

		protected EventScene(ActivityEvent activityEvent) {
			ActivityEvent = activityEvent;
		}

		#endregion

		/************************************************************************/
		#region Serialization

		public static EventScene FromJson(JsonData root, WorldState world) {
			ActivityEventType type = (ActivityEventType)System.Enum.Parse(typeof(ActivityEventType), (string)root["type"], true);
			string activityId = (string)root["activityId"];
			int eventIndex = (int)root["eventIndex"];
			Activity activity = world.GetActivity(activityId);
			ActivityEvent activityEvent = activity.GetEvent(eventIndex);

			switch(type) {
			case ActivityEventType.Battle:
				return new Scenes.BattleEventScene(activityEvent, world);
			default:
				Debug.LogError("Unsupported activity event scene: " + type.ToString());
				return null;
			}
		}

		public JsonData ToJson() {
			JsonData root = new JsonData() {
				["type"] = Type.ToString(),
				["activityId"] = ActivityEvent.Activity.ID,
				["eventIndex"] = ActivityEvent.EventIndex,
			};

			FillSaveData(root);

			return root;
		}

		protected abstract void FillSaveData(JsonData root);

		#endregion

		/************************************************************************/
		#region Privates

		private IEnumerator WaitForOperation(AsyncOperation op) {
			while(!op.isDone) {
				yield return null;
			}
		}

		private bool isLoaded;

		#endregion

	}

}
