﻿using System;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using UnityEngine;
using Activities.Events.Scenes.Battle;

namespace Activities.Events {

	public class BattleActivityEvent : ActivityEvent {

		public override ActivityEventType Type { get { return ActivityEventType.Battle; } }
		public override bool CanIntervene { get { return true; } }

		public override void AutoResolve(WorldState world) {
			DoBattle();
		}

		public int GetNumEnemies() {
			return enemies.Count;
		}

		public Hero GetEnemy(int index) {
			return enemies[index];
		}

		public BattleLogic SpawnBattleLogic() {
			return new BattleLogic(this);
		}

		/************************************************************************/
		#region Construction
			
		/// <summary>
		/// Used for creating a new event (see QuestActivity.Create()).
		/// </summary>
		/// <param name="eventDefinition"></param>
		/// <param name="triggerProgress"></param>
		/// <param name="world"></param>
		public BattleActivityEvent(ActivityEventDefinition eventDefinition, int triggerProgress, WorldState world) :
				base(eventDefinition.EventIndex, triggerProgress, eventDefinition.TriggerDuration, ActivityResult.Incomplete) {
			enemies = new List<Hero>();
			
			// Read the enemies from data!
			if(eventDefinition.Data.Keys.Contains("enemies")) {
				JsonData element = eventDefinition.Data["enemies"];
				Dictionary<MantleDefinition, int> raceCount = new Dictionary<MantleDefinition, int>();
				for(int i = 0; i < element.Count; ++i) {
					MantleDefinition race = world.GetMantleDefinition((string)element[i]["race"]);
					if(!raceCount.ContainsKey(race)) {
						raceCount[race] = 0;
					}
					string name = string.Format("{0} {1}", race.Name.ToString(), (char)('A' + raceCount[race]));
					++raceCount[race];
					Hero enemy = HeroGenerator.Generate(world, HeroOwner.NPC, race, null, name);
					enemies.Add(enemy);
				}
			}
		}

		/// <summary>
		/// Used for loading an event.
		/// </summary>
		/// <param name="triggerTime"></param>
		/// <param name="triggerDuration"></param>
		/// <param name="result"></param>
		/// <param name="root"></param>
		/// <param name="world"></param>
		public BattleActivityEvent(int eventIndex, int triggerTime, int triggerDuration, ActivityResult result, JsonData root, WorldState world) :
				base(eventIndex, triggerTime, triggerDuration, result) {
			enemies = new List<Hero>();
			if(root.Keys.Contains("enemies")) {
				JsonData element = root["enemies"];
				for(int i = 0; i < element.Count; ++i) {
					Hero enemy = Hero.FromJson(element[i], world);
					enemies.Add(enemy);
				}
			}
		}

		#endregion

		/************************************************************************/
		#region Serialization

		protected override void FillSaveData(JsonData root) {
			JsonData enemiesGroup = new JsonData();
			enemiesGroup.SetJsonType(JsonType.Array);
			for(int i = 0; i < enemies.Count; ++i) {
				enemiesGroup.Add(enemies[i].ToJson());
			}

			root["enemies"] = enemiesGroup;
		}

		#endregion

		/************************************************************************/
		#region Privates

		private void DoBattle() {
			BattleLogic battle = SpawnBattleLogic();

			Result = battle.AutoResolve();
		}

		private List<Hero> enemies;

		#endregion

	}

}
