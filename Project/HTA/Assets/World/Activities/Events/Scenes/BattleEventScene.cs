﻿using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using Activities.Events.Scenes.Battle;

namespace Activities.Events.Scenes {

	public class BattleEventScene : EventScene {

		public override ActivityEventType Type { get { return ActivityEventType.Battle; } }
		public override string SceneName { get { return "BattleEventScene"; } }

		public BattleLogic Data { get { return battleLogic; } }

		public void CompleteBattle(ActivityResult result, WorldState world) {
			ActivityEvent.Result = result;
			ActivityEvent.Activity.ResolveEvent();
			CompleteScene("WorldHub", world);
		}

		/************************************************************************/
		#region Construction

		/// <summary>
		/// Called when deserializing. See WorldState.ParseJson().
		/// </summary>
		public BattleEventScene(ActivityEvent activityEvent, JsonData root, WorldState world) :
				base(activityEvent) {
			battleLogic = null;
			if(root.Keys.Contains("data")) {
				battleLogic = BattleLogic.FromJson(activityEvent, root["data"], world);
			}
		}

		/// <summary>
		/// Called when spawning a new event scene. See WorldState.SpawnEventScene().
		/// </summary>
		public BattleEventScene(ActivityEvent activityEvent, WorldState world) :
				base(activityEvent) {
			battleLogic = (activityEvent as BattleActivityEvent).SpawnBattleLogic();
		}

		#endregion

		/************************************************************************/
		#region Serialization

		protected override void FillSaveData(JsonData root) {
			root["data"] = battleLogic.ToJson();
		}

		#endregion

		/************************************************************************/
		#region Privates

		private BattleLogic battleLogic;

		#endregion

	}

}
