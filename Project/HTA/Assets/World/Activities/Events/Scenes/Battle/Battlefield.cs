﻿using Priority_Queue;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Activities.Events.Scenes.Battle {

	public class Battlefield : MonoBehaviour {

		private BattleLogic battle;
		private BattleEventScene eventScene;
		private BattleActivityEvent battleEvent;

		[SerializeField]
		private BattleHeroItem[] allyItems;
		[SerializeField]
		private BattleHeroItem[] enemyItems;

		[SerializeField]
		private BattleSpellList spellList;

		private Dictionary<Hero, BattleHeroItem> itemMap = new Dictionary<Hero, BattleHeroItem>();
		private Hero selectedTarget = null;
		private SpellDefinition selectedSpell = null;

		private void Start() {
			if(WorldManager.World == null) {
				// Probably only happens in the editor.
				return;
			}

			spellList.gameObject.SetActive(false);

			itemMap = new Dictionary<Hero, BattleHeroItem>();

			eventScene = WorldManager.World.ActiveEventScene as BattleEventScene;
			battle = eventScene.Data;
			battleEvent = eventScene.ActivityEvent as BattleActivityEvent;
			
			int index = 0;
			for(; index < battle.AllyParty.Count; ++index) {
				Hero hero = battle.AllyParty[index];
				if(hero != null) {
					allyItems[index].ShowInfo(hero, true);
					allyItems[index].SetHighlighted(false);
					allyItems[index].SetSelectable(false);
					allyItems[index].gameObject.SetActive(true);

					itemMap[hero] = allyItems[index];
				}
				else {
					allyItems[index].gameObject.SetActive(false);
				}
			}
			for(; index < allyItems.Length; ++index) {
				allyItems[index].gameObject.SetActive(false);
			}

			index = 0;
			for(; index < battle.EnemyParty.Count; ++index) {
				Hero hero = battle.EnemyParty[index];
				if(hero != null) {
					enemyItems[index].ShowInfo(hero, false);
					enemyItems[index].SetHighlighted(false);
					enemyItems[index].SetSelectable(false);
					enemyItems[index].gameObject.SetActive(true);

					itemMap[hero] = enemyItems[index];
				}
				else {
					enemyItems[index].gameObject.SetActive(false);
				}
			}
			for(; index < enemyItems.Length; ++index) {
				enemyItems[index].gameObject.SetActive(false);
			}

			StartCoroutine(DoBattle());
		}

		private void OnEnable() {
			Hero.OnSelected += OnHeroSelected;
			spellList.OnSpellSelected += OnSpellSelected;
		}

		private void OnDisable() {
			Hero.OnSelected -= OnHeroSelected;
			spellList.OnSpellSelected -= OnSpellSelected;
		}

		private void OnHeroSelected(Hero hero) {
			selectedTarget = hero;
		}

		private void ShowSpellList(Hero hero) {
			spellList.InitializeList(hero);
			spellList.gameObject.SetActive(true);
		}

		private void HideSpellList() {
			spellList.gameObject.SetActive(false);
		}

		public void OnSpellSelected(SpellDefinition spell) {
			selectedSpell = spell;
		}

		private void UpdateTargets(List<Hero> targets) {
			foreach(var item in itemMap.Keys) {
				if(targets != null && targets.Contains(item)) {
					itemMap[item].SetSelectable(true);
				}
				else {
					itemMap[item].SetSelectable(false);
				}
			}
		}

		private IEnumerator DoBattle() {
			ActivityResult result = ActivityResult.Incomplete;
			while(result == ActivityResult.Incomplete) {
				// Select next hero to act.
				Hero hero = battle.GetNextTurnHero();
				BattleHeroItem heroItem = itemMap[hero];

				heroItem.SetHighlighted(true);

				List<Hero> targets = new List<Hero>();
				if(hero.Owner == HeroOwner.Player) {
					ShowSpellList(hero);
					selectedTarget = null;
					selectedSpell = null;
					while(selectedTarget == null) {
						while(selectedSpell == null) {
							yield return null;
						}
						
						targets.Clear();
						battle.GetValidTargets(hero, selectedSpell, targets);
						UpdateTargets(targets);

						SpellDefinition oldSpell = selectedSpell;
						while(selectedSpell == oldSpell &&
							(selectedTarget == null || !targets.Contains(selectedTarget))) {
							yield return null;
						}
					}
				}
				else {
					targets.Clear();
					List<SpellDefinition> spells = hero.GetAvailableSpells(WorldManager.World);
					selectedSpell = spells[Random.Range(0, spells.Count)];
					battle.GetValidTargets(hero, selectedSpell, targets);
					if(targets.Count == 0) {
						selectedSpell = null;
						selectedTarget = null;
					}
					else {
						selectedTarget = targets[Random.Range(0, targets.Count)];
					}
				}

				HideSpellList();
				UpdateTargets(null);

				int oldHealth = selectedTarget != null ? selectedTarget.Stats.Health : 0;
				battle.PerformAction(hero, selectedSpell, selectedTarget);
				if(selectedTarget != null) {
					int healthDelta = selectedTarget.Stats.Health - oldHealth;
					if(healthDelta > 0) {
						itemMap[selectedTarget].ShowHeal(healthDelta);
					}
					else if(healthDelta < 0) {
						itemMap[selectedTarget].ShowHurt(healthDelta);
					}
				}

				yield return new WaitForSeconds(0.5f);

				heroItem.SetHighlighted(false);

				result = battle.GetCurrentResult();
			}

			yield return null;

			string caption = null;
			string message = null;
			if(result == ActivityResult.Success) {
				caption = "VICTORY!";
				message = "The party is victorious in battle!";
			}
			else {
				caption = "DEFEAT!";
				message = "The party is defeated in battle!";
			}
			Popup.ShowPopup(
				caption, message,
				Popup.Button.OK,
				(popupResult) => {
					eventScene.CompleteBattle(result, WorldManager.World);
				});
		}

	}

}
