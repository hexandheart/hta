﻿using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Activities.Events.Scenes.Battle {

	public class BattleLogic {
		
		public BattleActivityEvent ActivityEvent { get; private set; }

		public List<Hero> AllyParty { get { return allyParty; } }
		public List<Hero> EnemyParty { get { return enemyParty; } }
		public List<Hero> TurnOrder { get { return turnOrder; } }

		public ActivityResult AutoResolve() {
			ActivityResult result = ActivityResult.Incomplete;
			while(result == ActivityResult.Incomplete) {
				Hero hero = GetNextTurnHero();
				List<SpellDefinition> spells = hero.GetAvailableSpells(WorldManager.World);
				SpellDefinition selectedSpell = spells[Random.Range(0, spells.Count)];
				List<Hero> targets = new List<Hero>();
				GetValidTargets(hero, selectedSpell, targets);
				if(targets.Count > 0) {
					int targetIndex = Random.Range(0, targets.Count);
					PerformAction(hero, selectedSpell, targets[targetIndex]);
				}
				else {
					PerformAction(hero, null, null);
				}

				result = GetCurrentResult();
			}

			return result;
		}

		public Hero GetNextTurnHero() {
			return turnOrder[0];
		}

		public void GetValidTargets(Hero source, SpellDefinition spell, List<Hero> targets) {
			var sourceAllyParty = source.Owner == HeroOwner.Player ? allyParty : enemyParty;
			var sourceEnemyParty = source.Owner == HeroOwner.Player ? enemyParty : allyParty;

			// Can we target enemies?
			if(spell.CanTargetEnemy) {
				foreach(var hero in sourceEnemyParty) {
					if(hero.Stats.Health > 0) {
						targets.Add(hero);
					}
				}
			}

			// Can we target allies or ourselves?
			if(spell.CanTargetAlly) {
				foreach(var hero in sourceAllyParty) {
					if(hero.Stats.Health > 0) {
						if(hero == source && spell.CanTargetSelf ||
							spell.CanTargetAlly) {
							targets.Add(hero);
						}
					}
				}
			}
		}

		public void PerformAction(Hero source, SpellDefinition spell, Hero target) {
			if(target != null && spell != null) {
				int range = spell.Spread * spell.Potency / 100;
				int potency = spell.Potency + Random.Range(-range, range + 1);
				target.Stats.Health += potency;

				if(target.Stats.Health <= 0) {
					target.Stats.Health = 0;
					turnOrder.Remove(target);
				}
				else if(target.Stats.Health > target.BaseStats.Health) {
					target.Stats.Health = target.BaseStats.Health;
				}
			}

			Debug.LogFormat("{0}: {1} => {2}",
				source.Name,
				spell == null ? "-" : spell.Name,
				target == null ? "-" : target.Name);

			turnOrder.Remove(source);
			turnOrder.Add(source);
		}

		public bool IsPartyAlive(List<Hero> party) {
			foreach(var hero in party) {
				if(hero.Stats.Health > 0) {
					return true;
				}
			}

			return false;
		}

		public ActivityResult GetCurrentResult() {
			if(!IsPartyAlive(allyParty)) {
				return ActivityResult.Failure;
			}
			else if(!IsPartyAlive(enemyParty)) {
				return ActivityResult.Success;
			}
			else {
				return ActivityResult.Incomplete;
			}
		}
		
		/************************************************************************/
		#region Construction

		public BattleLogic(ActivityEvent activityEvent) {
			ActivityEvent = activityEvent as BattleActivityEvent;

			// We'll need this for initializing the turn order below.
			List<Hero> heroesToAdd = new List<Hero>();

			// Get ally party members.
			allyParty = new List<Hero>();
			for(int i = 0; i < ActivityEvent.Activity.GetNumHeroes(); ++i) {
				Hero hero = ActivityEvent.Activity.GetHero(i);
				if(hero == null) {
					continue;
				}
				allyParty.Add(hero);
				heroesToAdd.Add(hero);
			}

			// Get enemy party members.
			enemyParty = new List<Hero>();
			for(int i = 0; i < ActivityEvent.GetNumEnemies(); ++i) {
				Hero hero = ActivityEvent.GetEnemy(i);
				if(hero == null) {
					continue;
				}
				enemyParty.Add(hero);
				heroesToAdd.Add(hero);
			}

			// Randomly fill in our turn order.
			turnOrder = new List<Hero>();
			for(int i = heroesToAdd.Count - 1; i >= 0; --i) {
				int index = Random.Range(0, i);
				turnOrder.Add(heroesToAdd[index]);
				heroesToAdd.RemoveAt(index);
			}
		}

		private BattleLogic(ActivityEvent activityEvent,
				List<Hero> allyParty, List<Hero> enemyParty, List<Hero> turnOrder) {
			ActivityEvent = activityEvent as BattleActivityEvent;

			this.allyParty = allyParty;
			this.enemyParty = enemyParty;
			this.turnOrder = turnOrder;
		}

		#endregion

		/************************************************************************/
		#region Serlialization

		public static BattleLogic FromJson(ActivityEvent activityEvent, JsonData root, WorldState world) {
			List<Hero> allyParty = null;
			List<Hero> enemyParty = null;
			List<Hero> turnOrder = null;

			return new BattleLogic(activityEvent, allyParty, enemyParty, turnOrder);
		}

		public JsonData ToJson() {
			JsonData allyPartyGroup = new JsonData();
			allyPartyGroup.SetJsonType(JsonType.Array);
			foreach(var hero in allyParty) {
				allyPartyGroup.Add(hero.ID);
			}

			JsonData enemyPartyGroup = new JsonData();
			enemyPartyGroup.SetJsonType(JsonType.Array);
			foreach(var hero in enemyParty) {
				enemyPartyGroup.Add(hero.ID);
			}

			JsonData turnOrderGroup = new JsonData();
			turnOrderGroup.SetJsonType(JsonType.Array);
			foreach(var hero in turnOrder) {
				turnOrderGroup.Add(hero.ID);
			}

			return new JsonData() {
				["allyParty"] = allyPartyGroup,
				["enemyParty"] = enemyPartyGroup,
				["turnOrder"] = turnOrderGroup,
			};
		}

		#endregion

		/************************************************************************/
		#region Privates

		private List<Hero> allyParty;
		private List<Hero> enemyParty;
		private List<Hero> turnOrder;

		#endregion

	}

}
