﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Activities.Events.Scenes.Battle {

	public class BattleHeroItem : MonoBehaviour {

		[SerializeField]
		private HeroCard heroCard;

		[SerializeField]
		private LabeledWidget combatTextLabel;

		private Hero hero;
		private Animator animator;
		private CanvasGroup canvasGroup;

		private void Awake() {
			animator = GetComponent<Animator>();
			canvasGroup = GetComponent<CanvasGroup>();
		}

		private void Update() {
			if(hero == null) {
				return;
			}
			UpdateState();
		}

		public void ShowInfo(Hero hero, bool isAlly) {
			this.hero = hero;
			heroCard.ShowInfo(hero);
		}

		public void SetHighlighted(bool isSelected) {
			heroCard.SetSelected(isSelected);
		}

		public void SetSelectable(bool isSelectable) {
			if(isSelectable) {
				canvasGroup.interactable = true;
				canvasGroup.alpha = 1f;
			}
			else {
				canvasGroup.interactable = false;
				canvasGroup.alpha = 1f;
			}
		}

		public void ShowHurt(int healthDelta) {
			combatTextLabel.Text = string.Format("{0}", -healthDelta);
			animator.SetTrigger("Hurt");
		}

		public void ShowHeal(int healthDelta) {
			combatTextLabel.Text = string.Format("+{0}", healthDelta);
			animator.SetTrigger("Heal");
		}

		private void UpdateState() {
			animator.SetBool("IsAlive", hero.Stats.Health > 0);
		}

	}

}
