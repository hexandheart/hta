﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Activities.Events.Scenes.Battle {

	public class BattleTest : MonoBehaviour {

		private BattleEventScene eventScene;

		private void Awake() {
			if(WorldManager.World == null) {
				return;
			}
			eventScene = WorldManager.World.ActiveEventScene as BattleEventScene;
		}

		public void OnCompleteClicked() {
			eventScene.CompleteBattle(ActivityResult.Success, WorldManager.World);
		}

	}

}

