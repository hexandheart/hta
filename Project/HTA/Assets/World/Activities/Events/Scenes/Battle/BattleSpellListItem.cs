﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Activities.Events.Scenes.Battle {

	public class BattleSpellListItem : MonoBehaviour {

		[SerializeField]
		private LabeledWidget label;

		[SerializeField]
		private TooltipDataProvider tooltipData;

		[SerializeField]
		private GameObject selectedImage;

		private Hero hero;
		private SpellDefinition spell;

		public void ShowInfo(Hero hero, SpellDefinition spell) {
			this.hero = hero;
			this.spell = spell;

			label.Text = spell.Name;

			tooltipData.SetTooltipData(string.Format("{0}\n\n{1}", spell.Name, spell.GetDescription()));
		}

		public void OnClicked() {
			GetComponentInParent<BattleSpellList>().OnSpellClicked(spell);
		}

		public void SetSelected(bool isSelected) {
			selectedImage.SetActive(isSelected);
		}

	}

}
