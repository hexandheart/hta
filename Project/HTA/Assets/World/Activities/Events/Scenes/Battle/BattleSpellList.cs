﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Activities.Events.Scenes.Battle {

	public class BattleSpellList : MonoBehaviour {

		public event SpellDefinition.SpellHandler OnSpellSelected;

		[SerializeField]
		private LayoutGroup itemLayout;

		[SerializeField]
		private BattleSpellListItem itemPrefab;

		private Dictionary<SpellDefinition, BattleSpellListItem> itemMap;
		private SpellDefinition selectedSpell;

		private void ClearList() {
			selectedSpell = null;
			for(int i = itemLayout.transform.childCount - 1; i >= 0; --i) {
				Transform t = itemLayout.transform.GetChild(i);
				Destroy(t.gameObject);
			}
			itemMap = new Dictionary<SpellDefinition, BattleSpellListItem>();
		}

		public BattleSpellListItem CreateItem(Hero hero, SpellDefinition spell) {
			BattleSpellListItem item = Instantiate(itemPrefab, itemLayout.transform);
			item.ShowInfo(hero, spell);
			itemMap[spell] = item;
			return item;
		}

		public void InitializeList(Hero hero) {
			ClearList();
			foreach(var spell in hero.GetAvailableSpells(WorldManager.World)) {
				CreateItem(hero, spell);
			}
		}

		public void OnSpellClicked(SpellDefinition spell) {
			OnSpellSelected?.Invoke(spell);
			if(selectedSpell != null) {
				itemMap[selectedSpell].SetSelected(false);
			}
			itemMap[spell].SetSelected(true);
			selectedSpell = spell;
		}

	}

}
