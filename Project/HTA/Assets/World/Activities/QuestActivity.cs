﻿using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Activities {

	public class QuestActivity : Activity {

		public override ActivityType Type { get { return ActivityType.Quest; } }
		public override string Name { get { return Quest.Name; } }

		public string QuestID { get; private set; }
		public Quest Quest { get; private set; }

		public override void OnTick(WorldState world) {
		}

		public override void OnComplete(WorldState world, ActivityResult result) {
			world.CompleteQuest(Quest, result);
		}

		/************************************************************************/
		#region Construction

		public static QuestActivity Create(Quest quest, WorldState world) {
			string id = Guid.NewGuid().ToString();
			int startTime = world.CurrentTime;
			int currentProgress = 0;
			int targetProgress = 0;
			Hero[] heroes = new Hero[quest.GetNumSlots()];
			for(int i = 0; i < quest.GetNumSlots(); ++i) {
				heroes[i] = quest.GetHeroInSlot(i);
			}

			// Prepare activity events.
			List<ActivityEvent> activityEvents = new List<ActivityEvent>();
			foreach(var eventDefinition in quest.Template.ActivityEvents) {
				int roll = UnityEngine.Random.Range(0, 100);
				if(roll >= eventDefinition.TriggerChance) {
					continue;
				}

				int triggerProgress = UnityEngine.Random.Range(eventDefinition.MinProgress, eventDefinition.MaxProgress + 1);

				ActivityEvent activityEvent = null;
				switch(eventDefinition.Type) {
				case ActivityEventType.RandomRoll:
					activityEvent = new Events.RandomRollActivityEvent(eventDefinition, triggerProgress, world);
					break;
				case ActivityEventType.Battle:
					activityEvent = new Events.BattleActivityEvent(eventDefinition, triggerProgress, world);
					break;
				}
				activityEvents.Add(activityEvent);
				targetProgress = triggerProgress;
			}

			targetProgress += quest.Template.FinishDuration;

			return new QuestActivity(id, startTime, currentProgress, targetProgress, heroes, activityEvents, quest);
		}

		public QuestActivity(
				string id, int startTime,
				int currentProgress, int targetProgress, int finishDuration,
				Hero[] heroes,
				List<ActivityEvent> activityEvents, int currentEventIndex, int currentEventTime,
				JsonData root, WorldState world) :
				base(id, startTime, currentProgress, targetProgress, finishDuration, heroes, activityEvents, currentEventIndex, currentEventTime) {
			QuestID = (string)root["questId"];
			Quest = world.GetQuest(QuestID);
			if(Quest == null) {
				Debug.LogErrorFormat("Unable to find associated POI quest for activity {0}", ID);
			}
			else {
				Quest.Activity = this;
			}
		}

		private QuestActivity(
				string id, int startTime,
				int currentProgress, int targetProgress,
				Hero[] heroes, List<ActivityEvent> activityEvents,
				Quest quest) :
				base(id, startTime, currentProgress, targetProgress, quest.Template.FinishDuration, heroes, activityEvents, -1, 0) {
			QuestID = quest.ID;
			Quest = quest;
			Quest.Activity = this;
		}

		#endregion

		/************************************************************************/
		#region Serialization

		protected override void FillSaveData(JsonData root) {
			root["questId"] = QuestID;
		}

		#endregion

	}

}
