﻿using Activities.Events;
using Activities.Events.Scenes;
using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Activities {

	public abstract class ActivityEvent {

		public abstract ActivityEventType Type { get; }
		public abstract bool CanIntervene { get; }

		public Activity Activity { get; set; }
		public int EventIndex { get; private set; }

		public int TriggerProgress { get; private set; }
		public int TriggerDuration { get; private set; }

		public ActivityResult Result { get; set; }

		public abstract void AutoResolve(WorldState world);

		public EventScene SpawnEventScene(WorldState world) {
			switch(Type) {
			case ActivityEventType.Battle:
				return new BattleEventScene(this, world);
			default:
				return null;
			}
		}

		/************************************************************************/
		#region Construction

		protected ActivityEvent(int eventIndex, int triggerProgress, int triggerDuration, ActivityResult result) {
			EventIndex = eventIndex;
			TriggerProgress = triggerProgress;
			TriggerDuration = triggerDuration;
			Result = result;
		}

		#endregion

		/************************************************************************/
		#region Serialization

		public static ActivityEvent FromJson(JsonData root, int eventIndex, WorldState world) {
			ActivityEventType type = (ActivityEventType)System.Enum.Parse(typeof(ActivityEventType), (string)root["type"], true);
			int triggerProgress = (int)root["triggerProgress"];
			int triggerDuration = (int)root["triggerDuration"];
			ActivityResult result = (ActivityResult)System.Enum.Parse(typeof(ActivityResult), (string)root["result"], true);

			switch(type) {
			case ActivityEventType.RandomRoll:
				return new RandomRollActivityEvent(eventIndex, triggerProgress, triggerDuration, result, root, world);
			case ActivityEventType.Battle:
				return new BattleActivityEvent(eventIndex, triggerProgress, triggerDuration, result, root, world);
			}

			return null;
		}

		public JsonData ToJson() {
			JsonData root = new JsonData() {
				["type"] = Type.ToString(),
				["triggerProgress"] = TriggerProgress,
				["triggerDuration"] = TriggerDuration,
				["result"] = Result.ToString(),
			};

			FillSaveData(root);

			return root;
		}

		protected abstract void FillSaveData(JsonData root);

		#endregion

	}

}
