﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public enum HeroOwner {
	Player,
	NPC,
}

public class Hero {

	public static readonly string JoblessID = "jobless";

	public delegate void HeroHandler(Hero hero);
	public delegate void HeroPoiHandler(Hero hero, Poi poi);

	public static event HeroHandler OnSelected;

	public string ID { get; private set; }
	public string Name { get; private set; }

	public HeroOwner Owner { get; set; }

	public MantleDefinition Race { get; private set; }
	public MantleDefinition Job { get; private set; }

	public Skill CoreSkill { get; private set; }

	public List<Skill> Skills { get { return skills; } }

	public Stats BaseStats { get; private set; }
	public Stats Stats { get; private set; }

	public int GetHireCostGold() {
		int cost = Race.HireCostGold;
		if(Job != null) {
			cost += Job.HireCostGold;
		}
		return cost;
	}

	public List<SpellDefinition> GetAvailableSpells(WorldState world) {
		List<SpellDefinition> spells = new List<SpellDefinition>();
		foreach(var skill in skills) {
			skill.GetAvailableSpells(spells, world);
		}
		return spells;
	}

	public int GetLevel() {
		if(CoreSkill == null) {
			return 0;
		}

		return CoreSkill.GetLevel();
	}

	/************************************************************************/
	#region Events

	public void TriggerSelected() {
		OnSelected?.Invoke(this);
	}

	#endregion

	/************************************************************************/
	#region Construction

	public Hero(
			string id, string name,
			HeroOwner owner,
			MantleDefinition race, MantleDefinition job,
			List<Skill> skills, Stats stats) {
		ID = id;
		Name = name;
		Owner = owner;
		Race = race;
		Job = job;
		this.skills = skills;
		Stats = stats;

		if(job != null && job.CoreSkillID != null) {
			CoreSkill = skills.Find(s => s.Definition.ID == job.CoreSkillID);
		}

		if(CoreSkill == null && race.CoreSkillID != null) {
			CoreSkill = skills.Find(s => s.Definition.ID == race.CoreSkillID);
		}

		UpdateBaseStats();
	}

	public Hero(
			string id, string name,
			HeroOwner owner,
			MantleDefinition race, MantleDefinition job,
			List<Skill> skills) {
		ID = id;
		Name = name;
		Owner = owner;
		Race = race;
		Job = job;
		this.skills = skills;

		if(job != null && job.CoreSkillID != null) {
			CoreSkill = skills.Find(s => s.Definition.ID == job.CoreSkillID);
		}

		if(CoreSkill == null && race.CoreSkillID != null) {
			CoreSkill = skills.Find(s => s.Definition.ID == race.CoreSkillID);
		}

		UpdateBaseStats();
		Stats = new Stats(BaseStats);
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static readonly string GroupName = "heroes";

	public static Hero FromJson(JsonData root, WorldState world) {
		string id = (string)root["id"];

		string name = id;
		if(root.Keys.Contains("name")) {
			name = (string)root["name"];
		}

		HeroOwner owner = HeroOwner.Player;
		if(root.Keys.Contains("owner")) {
			owner = (HeroOwner)System.Enum.Parse(typeof(HeroOwner), (string)root["owner"], true);
		}

		MantleDefinition race = null;
		if(root.Keys.Contains("race")) {
			race = world.GetMantleDefinition((string)root["race"]);
		}

		MantleDefinition job = null;
		if(root.Keys.Contains("job")) {
			job = world.GetMantleDefinition((string)root["job"]);
		}

		List<Skill> skills = new List<Skill>();
		if(root.Keys.Contains("skills")) {
			JsonData element = root["skills"];
			for(int i = 0; i < element.Count; ++i) {
				skills.Add(Skill.FromJson(element[i], world));
			}
		}

		Stats stats = null;
		if(root.Keys.Contains("stats")) {
			stats = Stats.FromJson(root["stats"]);
		}
		else {
			stats = new Stats();
		}

		return new Hero(id, name, owner, race, job, skills, stats);
	}

	public JsonData ToJson() {
		JsonData skillsGroup = new JsonData();
		skillsGroup.SetJsonType(JsonType.Array);
		foreach(var item in skills) {
			skillsGroup.Add(item.ToJson());
		}

		JsonData root = new JsonData {
			["id"] = ID,
			["name"] = Name,
			["owner"] = Owner.ToString(),
			["race"] = Race.ID,
			["skills"] = skillsGroup,
			["stats"] = Stats.ToJson(),
		};
		if(Job != null) {
			root["job"] = Job.ID;
		}

		return root;
	}

	#endregion

	/************************************************************************/
	#region Privates

	private List<Skill> skills;

	private void UpdateBaseStats() {
		if(Job != null) {
			BaseStats = Race.BaseStats + Job.BaseStats;
		}
		else {
			BaseStats = Race.BaseStats;
		}
	}

	#endregion

}
