﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class Quest {

	public delegate void QuestHandler(Quest quest);
	public static QuestHandler OnQuestSpawned;
	public QuestHandler OnQuestExpired;

	public string ID { get; private set; }

	public string Name { get; private set; }
	public string Info { get; private set; }
	public int RewardGold { get; private set; }

	public int SpawnTime { get; private set; }
	public int DespawnTime { get; private set; }

	public ActivityResult Result { get; set; }

	public ActivityTemplateDefinition Template { get; private set; }

	public int GetExpectedDuration() {
		int duration = 0;
		foreach(var e in Template.ActivityEvents) {
			duration += (e.MinProgress + e.MaxProgress + e.TriggerDuration) / 2;
		}
		return duration + Template.FinishDuration;
	}

	public int GetNumSlots() {
		return heroSlots.Length;
	}

	public Hero GetHeroInSlot(int slotIndex) {
		return heroSlots[slotIndex];
	}

	public void SetHeroInSlot(Hero hero, int slotIndex) {
		heroSlots[slotIndex] = hero;
	}

	public bool CanStartQuest() {
		bool canStart = false;

		// At least one slot must have a hero assigned.
		for(int i = 0; i < heroSlots.Length; ++i) {
			if(heroSlots[i] != null) {
				canStart = true;
				break;
			}
		}

		return canStart;
	}

	public bool IsActive() {
		return Activity != null;
	}

	public Activity Activity { get; set; }

	public void TriggerSpawned() {
		OnQuestSpawned?.Invoke(this);
	}

	public void TriggerExpired() {
		OnQuestExpired?.Invoke(this);
	}

	/************************************************************************/
	#region Construction

	public Quest(string id, string name, string info, int rewardGold, int spawnTime, int despawnTime, ActivityResult result, Hero[] heroSlots, ActivityTemplateDefinition template) {
		ID = id;
		Name = name;
		Info = info;
		RewardGold = rewardGold;
		SpawnTime = spawnTime;
		DespawnTime = despawnTime;
		Result = result;
		this.heroSlots = heroSlots;
		Template = template;
	}

	public Quest(Quest o) {
		ID = o.ID;
		Name = o.Name;
		Info = o.Info;
		RewardGold = o.RewardGold;
		SpawnTime = o.SpawnTime;
		DespawnTime = o.DespawnTime;
		Result = o.Result;
		heroSlots = new Hero[o.heroSlots.Length];
		for(int i = 0; i < heroSlots.Length; ++i) {
			heroSlots[i] = o.heroSlots[i];
		}
		Template = new ActivityTemplateDefinition(o.Template);
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static readonly string GroupName = "quests";

	public static Quest FromJson(JsonData root, WorldState world) {
		string id;
		if(root.Keys.Contains("id")) {
			id = (string)root["id"];
		}
		else {
			id = System.Guid.NewGuid().ToString();
		}

		string name = "Unnamed Quest";
		if(root.Keys.Contains("name")) {
			name = (string)root["name"];
		}

		string info = "";
		if(root.Keys.Contains("info")) {
			info = (string)root["info"];
		}

		int rewardGold = 0;
		if(root.Keys.Contains("reward")) {
			JsonData element = root["reward"];
			if(element.Keys.Contains("gold")) {
				rewardGold = (int)element["gold"];
			}
		}

		ActivityResult result = ActivityResult.Incomplete;
		if(root.Keys.Contains("result")) {
			result = (ActivityResult)System.Enum.Parse(typeof(ActivityResult), (string)root["result"], false);
		}

		int spawnTime = 0;
		int despawnTime = 0;
		if(root.Keys.Contains("spawn")) {
			JsonData element = root["spawn"];
			
			if(element.Keys.Contains("spawnTime")) {
				spawnTime = (int)element["spawnTime"];
			}
			
			if(element.Keys.Contains("despawnTime")) {
				despawnTime = (int)element["despawnTime"];
			}
		}

		Hero[] heroSlots = null;
		if(root.Keys.Contains("heroSlots")) {
			JsonData element = root["heroSlots"];
			heroSlots = new Hero[element.Count];
			for(int i = 0; i < element.Count; ++i) {
				if(element[i] == null) {
					continue;
				}

				heroSlots[i] = world.GetHero((string)element[i]);
			}
		}
		else {
			heroSlots = new Hero[0];
		}

		ActivityTemplateDefinition template = ActivityTemplateDefinition.Default;
		if(root.Keys.Contains("template")) {
			template = ActivityTemplateDefinition.FromJson(root["template"]);
		}

		return new Quest(id, name, info, rewardGold, spawnTime, despawnTime, result, heroSlots, template);
	}

	public JsonData ToJson() {
		JsonData slotsGroup = new JsonData();
		slotsGroup.SetJsonType(JsonType.Array);
		for(int i = 0; i < heroSlots.Length; ++i) {
			if(heroSlots[i] != null) {
				slotsGroup.Add(heroSlots[i].ID);
			}
			else {
				slotsGroup.Add(null);
			}
		}

		return new JsonData() {
			["id"] = ID,
			["name"] = Name,
			["info"] = Info,
			["reward"] = new JsonData() {
				["gold"] = RewardGold,
			},
			["result"] = Result.ToString(),
			["spawn"] = new JsonData() {
				["spawnTime"] = SpawnTime,
				["despawnTime"] = DespawnTime,
			},
			["heroSlots"] = slotsGroup,
			["template"] = Template.ToJson(),
		};
	}

	#endregion

	/************************************************************************/
	#region Privates

	private Hero[] heroSlots;

	#endregion

}
