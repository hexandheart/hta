﻿using AssetBundles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;
using System;
using Activities.Events;
using Activities;

public class WorldState {

	public static readonly int TicksPerDay = 10;
	public static readonly int DaysPerYear = 100;

	/************************************************************************/

	public CampaignDefinition GetCampaignDefinition() {
		foreach(var package in packages) {
			CampaignDefinition def = package.Campaign;
			if(def != null) {
				return def;
			}
		}
		return null;
	}

	public RegionDefinition GetRegionDefinition(string id) {
		foreach(var package in packages) {
			RegionDefinition def = package.GetRegionDefinition(id);
			if(def != null) {
				return def;
			}
		}

		return null;
	}

	public PoiDefinition GetPoiDefinition(string id) {
		foreach(var package in packages) {
			PoiDefinition def = package.GetPoiDefinition(id);
			if(def != null) {
				return def;
			}
		}

		return null;
	}

	public Poi GetPoi(string id) {
		foreach(var region in regions) {
			foreach(var poi in region.Pois) {
				if(poi.Definition.ID == id) {
					return poi;
				}
			}
		}

		return null;
	}

	public Region GetRegion(string id) {
		return regions.Find(r => r.Definition.ID == id);
	}

	public List<Region> Regions { get { return regions; } }

	public MantleDefinition GetMantleDefinition(string id) {
		foreach(var package in packages) {
			MantleDefinition def = package.GetMantleDefinition(id);
			if(def != null) {
				return def;
			}
		}

		return null;
	}

	public List<MantleDefinition> GetMantlesByType(MantleType type) {
		List<MantleDefinition> mantles = new List<MantleDefinition>();
		
		foreach(var package in packages) {
			mantles.AddRange(package.Mantles.FindAll(m => m.Type == type));
		}

		return mantles;
	}

	public SpellDefinition GetSpellDefinition(string id) {
		foreach(var package in packages) {
			SpellDefinition def = package.GetSpellDefinition(id);
			if(def != null) {
				return def;
			}
		}

		return null;
	}

	public SkillDefinition GetSkillDefinition(string id) {
		foreach(var package in packages) {
			SkillDefinition def = package.GetSkillDefinition(id);
			if(def != null) {
				return def;
			}
		}

		return null;
	}

	public XPTable GetXPTable(string id) {
		foreach(var package in packages) {
			XPTable def = package.GetXPTable(id);
			if(def != null) {
				return def;
			}
		}

		return null;
	}

	public int CurrentTime { get; private set; }

	public int GetDay() {
		return ((CurrentTime / TicksPerDay) % DaysPerYear) + 1;
	}

	public int GetYear() {
		return ((CurrentTime / TicksPerDay) / DaysPerYear) + 1;
	}

	public string GetDateLabel() {
		return string.Format("Day {0} Year {1}", GetDay(), GetYear());
	}

	public string GetTimeSpanLabel(int time) {
		if(time < TicksPerDay) {
			return "<1d";
		}
		else {
			return (time / TicksPerDay) + "d";
		}
	}

	public static int ParseTimeSpan(string timeSpan) {
		if(timeSpan.EndsWith("d")) {
			return (int.Parse(timeSpan.Substring(0, timeSpan.Length - 1))) * TicksPerDay;
		}
		return int.Parse(timeSpan);
	}

	public NameGenerator NameGenerator { get; private set; }

	public List<Hero> Heroes { get { return heroes; } }

	public void HireHero(Hero hero) {
		if(heroes.Find(h => h.ID == hero.ID) != null) {
			return;
		}
		// Gotta pay!
		Gold -= hero.GetHireCostGold();
		hero.Owner = HeroOwner.Player;
		heroes.Add(hero);
	}

	public Hero GetHero(string id) {
		return heroes.Find(h => h.ID == id);
	}

	public Quest GetHeroQuest(Hero hero) {
		foreach(var region in regions) {
			foreach(var poi in region.Pois) {
				Pois.QuestingPoi questPoi = poi as Pois.QuestingPoi;
				if(questPoi == null) {
					continue;
				}

				foreach(var quest in questPoi.Quests) {
					for(int i = 0; i < quest.GetNumSlots(); ++i) {
						if(quest.GetHeroInSlot(i) == hero) {
							return quest;
						}
					}
				}
			}
		}

		return null;
	}

	public Activity GetHeroActivity(Hero hero) {
		foreach(var activity in activities) {
			for(int i = 0; i < activity.GetNumHeroes(); ++i) {
				if(activity.GetHero(i) == hero) {
					return activity;
				}
			}
		}
		return null;
	}

	public Poi GetHeroPoi(Hero hero) {
		foreach(var region in regions) {
			foreach(var poi in region.Pois) {
				if(poi.IsHeroAssigned(hero)) {
					return poi;
				}
			}
		}
		return null;
	}

	public bool IsHeroBusy(Hero hero) {
		// Is the hero on an activity?
		if(GetHeroActivity(hero) != null) {
			return true;
		}

		// Is the hero in another poi?
		Poi poi = GetHeroPoi(hero);
		if(poi != null) {
			return true;
		}

		return false;
	}

	public Quest GetQuest(string id) {
		foreach(var region in regions) {
			foreach(var poi in region.Pois) {
				Pois.QuestingPoi questPoi = poi as Pois.QuestingPoi;
				if(questPoi == null) {
					continue;
				}

				Quest quest = questPoi.Quests.Find(q => q.ID == id);
				if(quest != null) {
					return quest;
				}
			}
		}

		return null;
	}

	public void StartQuest(Quest quest) {
		Activity activity = Activities.QuestActivity.Create(quest, this);
		activities.Add(activity);
		activity.TriggerStarted();
	}

	public void CompleteQuest(Quest quest, ActivityResult result) {
		Activity activity = quest.Activity;

		// Remove the activity.
		if(activity != null) {
			completedActivities.Add(activity);
		}

		quest.Activity = null;
		quest.Result = result;

		// If the quest has a reward, award it!
		if(result == ActivityResult.Success) {
			Gold += quest.RewardGold;
		}

		activity.TriggerCompleted();
	}

	public int Gold { get; set; }

	public List<Activity> CurrentActivities { get { return activities; } }

	public Activity GetActivity(string id) {
		return activities.Find(a => a.ID == id);
	}

	public EventScene ActiveEventScene { get { return activeEventScene; } }

	public void SpawnEventScene(ActivityEvent activityEvent) {
		Debug.AssertFormat(activeEventScene == null, "Attempting to start an event scene, but one is already active!");
		activeEventScene = activityEvent.SpawnEventScene(this);
		GameStateManager.OnGameStateChanged += activeEventScene.OnGameStateChanged;
		activeEventScene.LoadScene();
	}

	public void ClearEventScene() {
		Debug.AssertFormat(activeEventScene != null, "Attempting to clear an event scene, but none is already!");
		activeEventScene = null;
	}

	/************************************************************************/
	#region World ticking logic

	public void TickWorld() {
		if(activeEventScene != null) {
			return;
		}

		++CurrentTime;

		// Tick activities first!
		foreach(var activity in activities) {
			activity.Tick(this);
		}

		// Prune completed activities.
		foreach(var activity in completedActivities) {
			activities.Remove(activity);
		}
		completedActivities.Clear();

		foreach(var region in regions) {
			region.Tick(this);
		}
	}

	#endregion

	/************************************************************************/
	#region Framework stuff

	public WorldHeader Header { get; private set; }

	public string WorldName { get { return Header.WorldName; } }

	public string GetFileName() {
		return Header.GetWorldFileName();
	}

	public List<Package> Packages { get { return packages; } }

	public IEnumerator CreateAndLoadAsync(WorldHeader header) {
		Header = header;
		CurrentTime = 0;

		// XML won't exist yet. We need to initialize things first.
		yield return WorldManager.Instance.StartCoroutine(LoadPackages());

		// Starting gold!
		Gold = 0;
		CampaignDefinition campaign = GetCampaignDefinition();
		if(campaign != null) {
			Gold = campaign.StartingGold;
		}

		// Have to set up initial objects.
		regions = new List<Region>();
		foreach(var package in packages) {
			foreach(var definition in package.Regions) {
				Region region = new Region(definition, this);
				regions.Add(region);
			}
		}

		heroes = new List<Hero>();
		activities = new List<Activity>();

		TickWorld();
		
		initialized = true;
	}

	public IEnumerator LoadAsync(string filename, WorldHeader header) {
		Header = header;

		// Load the XML first and foremost.
		JsonData root = JsonMapper.ToObject(File.ReadAllText(filename));

		// Load packages.
		yield return WorldManager.Instance.StartCoroutine(LoadPackages());

		// Parse the XML.
		ParseJson(root, header);

		initialized = true;
	}

	#endregion

	/************************************************************************/
	#region Construction

	public WorldState() {
		initialized = false;
		NameGenerator = new NameGenerator();
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static readonly string ElementName = "worldState";

	private void ParseJson(JsonData root, WorldHeader header) {
		CurrentTime = 0;
		if(root.Keys.Contains("time")) {
			CurrentTime = (int)root["time"];
		}

		Gold = 0;
		if(root.Keys.Contains("currency")) {
			JsonData element = root["currency"];
			if(element.Keys.Contains("gold")) {
				Gold = (int)element["gold"];
			}
		}

		heroes = new List<Hero>();
		if(root.Keys.Contains(Hero.GroupName)) {
			JsonData element = root[Hero.GroupName];
			for(int i = 0; i < element.Count; ++i) {
				Hero hero = Hero.FromJson(element[i], this);
				heroes.Add(hero);
			}
		}

		regions = new List<Region>();
		if(root.Keys.Contains(Region.GroupName)) {
			JsonData element = root[Region.GroupName];
			for(int i = 0; i < element.Count; ++i) {
				Region region = Region.FromJson(element[i], this);
				regions.Add(region);
			}
		}

		// Ensure that every region in our packages has a regions entry.
		foreach(var package in packages) {
			foreach(var definition in package.Regions) {
				Region region = regions.Find(r => r.Definition.ID == definition.ID);
				if(region == null) {
					region = new Region(definition, this);
					regions.Add(region);
				}

				// Make sure each of the regions also has an entry for each poi.
				foreach(var poiDef in definition.Pois) {
					Poi poi = region.Pois.Find(p => p.Definition.ID == poiDef.ID);
					if(poi == null) {
						poi = Poi.CreateNew(poiDef, this);
						region.Pois.Add(poi);
					}
				}
			}
		}

		activities = new List<Activity>();
		if(root.Keys.Contains(Activity.GroupName)) {
			JsonData element = root[Activity.GroupName];
			for(int i = 0; i < element.Count; ++i) {
				Activity activity = Activity.FromJson(element[i], this);
				activities.Add(activity);
			}
		}

		activeEventScene = null;
		if(root.Keys.Contains("activeEventScene")) {
			activeEventScene = EventScene.FromJson(root["activeEventScene"], this);
			GameStateManager.OnGameStateChanged += activeEventScene.OnGameStateChanged;
		}
	}

	public JsonData ToJson() {
		JsonData heroesGroup = new JsonData();
		heroesGroup.SetJsonType(JsonType.Array);
		for(int i = 0; i < heroes.Count; ++i) {
			heroesGroup.Add(heroes[i].ToJson());
		}

		JsonData regionsGroup = new JsonData();
		regionsGroup.SetJsonType(JsonType.Array);
		for(int i = 0; i < regions.Count; ++i) {
			regionsGroup.Add(regions[i].ToJson());
		}

		JsonData activitiesGroup = new JsonData();
		activitiesGroup.SetJsonType(JsonType.Array);
		for(int i = 0; i < activities.Count; ++i) {
			activitiesGroup.Add(activities[i].ToJson());
		}

		JsonData root = new JsonData() {
			["name"] = WorldName,
			["salt"] = Header.WorldSalt,
			["time"] = CurrentTime,
			["currency"] = new JsonData() {
				["gold"] = Gold,
			},
			["regions"] = regionsGroup,
			["heroes"] = heroesGroup,
			["activities"] = activitiesGroup,
		};

		if(activeEventScene != null) {
			root["activeEventScene"] = activeEventScene.ToJson();
		}

		return root;
	}

	#endregion

	/************************************************************************/
	#region Privates

	private IEnumerator LoadPackages() {
		packages = new List<Package>();

		for(int packageIndex = 0; packageIndex < Header.PackageIDs.Count; ++packageIndex) {
			string packageId = Header.PackageIDs[packageIndex];

			// Load the package info asset bundle.
			AssetBundleLoadAssetOperation req = AssetBundleManager.LoadAssetAsync(packageId, "Info", typeof(TextAsset));
			if(req == null) {
				Debug.LogErrorFormat("Error loading package info from asset bundle {0}", packageId);
				yield break;
			}
			yield return WorldManager.Instance.StartCoroutine(req);

			TextAsset json = req.GetAsset<TextAsset>();
			PackageInfo info = null;
			try {
				info = PackageInfo.FromJson(JsonMapper.ToObject(json.text));
			}
			catch(Exception e) {
				Debug.LogErrorFormat("Error loading package info for '{0}': {1}", packageId, e.Message);
				yield break;
			}

			// Add dependencies!
			foreach(var dependency in info.Include) {
				if(!Header.PackageIDs.Contains(dependency)) {
					Header.PackageIDs.Add(dependency);
				}
			}

			// Load the package now.
			req = AssetBundleManager.LoadAssetAsync(packageId, "Package", typeof(TextAsset));
			if(req == null) {
				Debug.LogErrorFormat("Error loading package from asset bundle {0}", packageId);
				yield break;
			}
			yield return WorldManager.Instance.StartCoroutine(req);

			json = req.GetAsset<TextAsset>();
			JsonData root;
			Package package;
			try {
				root = JsonMapper.ToObject(json.text);
				package = Package.FromJson(root, info);
				packages.Add(package);

				// Extract NameGenerator stuff.
				if(root.Keys.Contains(NameGenerator.ElementName)) {
					NameGenerator.AddFromJson(root[NameGenerator.ElementName]);
				}
			}
			catch(Exception e) {
				Debug.LogErrorFormat("Error loading package '{0}': {1}", packageId, e.Message);
				yield break;
			}
		}
	}

	private bool initialized = false;
	private List<Package> packages;

	private List<Region> regions;
	private List<Hero> heroes;

	private List<Activity> activities;
	private List<Activity> completedActivities = new List<Activity>();

	private EventScene activeEventScene;

	#endregion

}
