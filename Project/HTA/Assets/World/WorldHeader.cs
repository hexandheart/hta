﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class WorldHeader {

	public string WorldName { get; private set; }
	public string WorldSalt { get; private set; }
	public List<string> PackageIDs { get; private set; }

	public string GetWorldFileName() {
		return WorldName.Replace(" ", string.Empty) + "_" + WorldSalt + ".json";
	}

	public string GetFileName() {
		return WorldName.Replace(" ", string.Empty) + "_" + WorldSalt + "_header.json";
	}

	/************************************************************************/
	#region Construction

	public WorldHeader(string worldName, List<string> packageIds) {
		WorldName = worldName;
		WorldSalt = System.Guid.NewGuid().ToString().Substring(0, 8);
		PackageIDs = packageIds;
		if(PackageIDs == null) {
			PackageIDs = new List<string>() {
				"campaign-core"
			};
		}
	}

	private WorldHeader(string worldName, string worldSalt, List<string> packageIds) {
		WorldName = worldName;
		WorldSalt = worldSalt;
		PackageIDs = packageIds;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static readonly string ElementName = "worldHeader";

	public static WorldHeader FromJson(JsonData root) {
		string worldName = (string)root["name"];
		string worldSalt = (string)root["salt"];

		List<string> packageIds = new List<string>();
		if(root.Keys.Contains("packages")) {
			JsonData element = root["packages"];
			for(int i = 0; i < element.Count; ++i) {
				packageIds.Add((string)element[i]);
			}
		}

		return new WorldHeader(worldName, worldSalt, packageIds);
	}

	public JsonData ToJson() {
		JsonData packagesElement = new JsonData();
		packagesElement.SetJsonType(JsonType.Array);
		for(int i = 0; i < PackageIDs.Count; ++i) {
			packagesElement.Add(PackageIDs[i]);
		}

		return new JsonData() {
			["name"] = WorldName,
			["salt"] = WorldSalt,
			["packages"] = packagesElement,
		};
	}

	#endregion

	/************************************************************************/
	#region Privates

	#endregion

}
