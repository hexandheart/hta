﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public abstract class Poi {

	public delegate void PoiHandler(Poi poi);

	public PoiDefinition Definition { get; private set; }

	public abstract bool IsHeroAssigned(Hero hero);
	public abstract void Tick(WorldState world);

	/************************************************************************/
	#region Construction

	protected Poi(PoiDefinition definition) {
		Definition = definition;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static readonly string GroupName = "pois";

	public static Poi FromJson(JsonData root, WorldState world) {
		string id = (string)root["id"];
		PoiDefinition definition = world.GetPoiDefinition(id);

		if(definition == null) {
			Debug.LogWarning("Error loading poi " + id);
			return null;
		}

		switch(definition.Type) {
		case PoiType.Questing:
			return new Pois.QuestingPoi(definition, root, world);
		case PoiType.Recruitment:
			return new Pois.RecruitmentPoi(definition, root, world);
		case PoiType.Recovery:
			return new Pois.RecoveryPoi(definition, root, world);
		}

		return null;
	}

	public static Poi CreateNew(PoiDefinition definition, WorldState world) {
		switch(definition.Type) {
		case PoiType.Questing:
			return new Pois.QuestingPoi(definition, null, world);
		case PoiType.Recruitment:
			return new Pois.RecruitmentPoi(definition, null, world);
		case PoiType.Recovery:
			return new Pois.RecoveryPoi(definition, null, world);
		}

		return null;
	}

	public JsonData ToJson() {
		JsonData root = new JsonData() {
			["id"] = Definition.ID,
		};

		FillSaveData(root);

		return root;
	}

	protected abstract void FillSaveData(JsonData root);

	#endregion

	/************************************************************************/
	#region Privates

	#endregion

}
