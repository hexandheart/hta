﻿using AssetBundles;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using LitJson;
using System;
using System.Text;

public class WorldManager : MonoBehaviour {

	public static WorldManager Instance { get { return instance; } }
	public static WorldState World { get { return world; } }

	public static SimSpeed SimSpeed {
		get {
			if(instance.pauseCount > 0) {
				return SimSpeed.Paused;
			}
			return instance.simSpeed;
		}
		set {
			if(instance.pauseCount > 0) {
				return;
			}
			instance.simSpeed = value;
		}
	}

	public static void PushPause() {
		++instance.pauseCount;
	}

	public static void PopPause() {
		--instance.pauseCount;
		Debug.Assert(instance.pauseCount >= 0, "Pause override reference count mismatch.");
	}

	/************************************************************************/
	#region Inspector tweakables

	[SerializeField]
	[Range(0f, 5f)]
	private float secondsPerTick;

	[SerializeField]
	private int ticksPerAutoSave;

	[SerializeField]
	private SimSpeed simSpeed;

	[SerializeField]
	private UIPalette uiPalette;

	public static UIPalette Palette { get { return instance.uiPalette; } }

	private float timeSinceLastTick = 0f;
	private int ticksSinceLastSave = 0;

	#endregion

	/************************************************************************/
	#region Save/load

	public delegate void WorldLoadedDelegate();
	public static event WorldLoadedDelegate OnWorldLoaded;

	public static List<WorldHeader> GetWorldList() {
		List<WorldHeader> headers = new List<WorldHeader>();
		if(!Directory.Exists(GetSavePath())) {
			return headers;
		}

		string[] files = Directory.GetFiles(GetSavePath());

		for(int i = 0; i < files.Length; ++i) {
			if(!files[i].EndsWith("_header.json")) {
				continue;
			}

			try {
				WorldHeader header = WorldHeader.FromJson(JsonMapper.ToObject(File.ReadAllText(files[i])));
				headers.Add(header);
			}
			catch(Exception e) {
				Debug.LogError("Error loading header: " + e.Message);
			}
		}

		return headers;
	}

	public static void CreateNewWorld(string worldName) {
		WorldHeader header = new WorldHeader(worldName, null);
		world = new WorldState();
		Instance.StartCoroutine(Instance.InitializeWorld(World.CreateAndLoadAsync(header), true));
	}

	public static void LoadWorld(WorldHeader header) {
		world = new WorldState();
		Instance.StartCoroutine(Instance.InitializeWorld(World.LoadAsync(GetSavePath() + header.GetWorldFileName(), header)));
	}

	public static void DeleteWorld(WorldHeader header) {
		File.Delete(GetSavePath() + header.GetFileName());
		File.Delete(GetSavePath() + header.GetWorldFileName());
	}
	#endregion

	/************************************************************************/
	#region Privates

	private void Start() {
		instance = this;
		DontDestroyOnLoad(gameObject);

		AssetBundleManager.Initialize();
	}

	private void OnApplicationQuit() {
		if(World != null) {
			SaveState();
		}
	}

	private void OnDestroy() {
		instance = null;
	}

	private void Update() {
		if(SimSpeed == SimSpeed.Paused) {
			timeSinceLastTick = 0f;
			return;
		}

		timeSinceLastTick += Time.deltaTime;
		if(timeSinceLastTick >= secondsPerTick) {
			timeSinceLastTick = 0f;
			world.TickWorld();

			if(ticksPerAutoSave > 0) {
				++ticksSinceLastSave;
				if(ticksSinceLastSave >= ticksPerAutoSave) {
					ticksSinceLastSave = 0;
					SaveState();
				}
			}
		}
	}

	private IEnumerator InitializeWorld(IEnumerator loadCoroutine, bool newGame = false) {
		yield return StartCoroutine(loadCoroutine);
		if(newGame) {
			SaveState();
		}
		OnWorldLoaded?.Invoke();
	}

	/************************************************************************/
	#region Save game stuff

	private static string GetSavePath() {
		return Application.persistentDataPath + "/Saves/";
	}

	private void SaveState() {
		StringBuilder sb = new StringBuilder();
		JsonWriter writer = new JsonWriter(sb) {
			PrettyPrint = true
		};

		// Ensure save folder exists.
		Directory.CreateDirectory(GetSavePath());

		writer.Reset();
		sb.Clear();
		World.Header.ToJson().ToJson(writer);
		File.WriteAllText(GetSavePath() + World.Header.GetFileName(), sb.ToString());

		writer.Reset();
		sb.Clear();
		World.ToJson().ToJson(writer);
		File.WriteAllText(GetSavePath() + World.GetFileName(), sb.ToString());
	}

	#endregion

	private static WorldManager instance;
	private static WorldState world;

	private int pauseCount;

	#endregion

}
