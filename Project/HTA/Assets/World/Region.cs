﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class Region {

	public delegate void RegionHandler(Region region);
	
	public RegionDefinition Definition { get; private set; }

	public List<Poi> Pois { get { return pois; } }

	public void Tick(WorldState world) {
		foreach(var poi in pois) {
			poi.Tick(world);
		}
	}

	/************************************************************************/
	#region Construction

	public Region(RegionDefinition definition, WorldState world) {
		Definition = definition;

		// Initialize pois!
		pois = new List<Poi>();
		foreach(var poiDef in Definition.Pois) {
			Poi poi = Poi.CreateNew(poiDef, world);
			pois.Add(poi);
		}
	}

	public Region(RegionDefinition definition, List<Poi> pois) {
		Definition = definition;
		this.pois = pois;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static readonly string GroupName = "regions";

	public static Region FromJson(JsonData root, WorldState world) {
		string id = (string)root["id"];
		RegionDefinition definition = world.GetRegionDefinition(id);

		List<Poi> pois = new List<Poi>();
		if(root.Keys.Contains(Poi.GroupName)) {
			JsonData element = root[Poi.GroupName];
			for(int i = 0; i < element.Count; ++i) {
				Poi poi = Poi.FromJson(element[i], world);
				if(poi.Definition != null) {
					pois.Add(poi);
				}
			}
		}

		return new Region(definition, pois);
	}

	public JsonData ToJson() {
		JsonData poiGroup = new JsonData();
		poiGroup.SetJsonType(JsonType.Array);
		for(int i = 0; i < pois.Count; ++i) {
			poiGroup.Add(pois[i].ToJson());
		}

		return new JsonData() {
			["id"] = Definition.ID,
			[Poi.GroupName] = poiGroup,
		};
	}

	#endregion

	/************************************************************************/
	#region Privates

	private List<Poi> pois;

	#endregion

}
