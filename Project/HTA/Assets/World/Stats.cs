﻿using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats {

	public static readonly Stats Default = new Stats();

	public int Health { get; set; }
	public int Power { get; set; }

	/************************************************************************/
	#region Construction

	private Stats(int health, int power) {
		Health = health;
		Power = power;
	}

	public Stats(Stats o) {
		Health = o.Health;
		Power = o.Power;
	}

	public Stats() {
		Health = 0;
		Power = 0;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static Stats FromJson(JsonData root) {
		int health = 0;
		if(root.Keys.Contains("health")) {
			health = (int)root["health"];
		}

		int power = 0;
		if(root.Keys.Contains("power")) {
			power = (int)root["power"];
		}

		return new Stats(health, power);
	}

	public JsonData ToJson() {
		return new JsonData() {
			["health"] = Health,
			["power"] = Power,
		};
	}

	#endregion

	/************************************************************************/
	#region Privates

	public static Stats operator +(Stats lhs, Stats rhs) {
		return new Stats(
			lhs.Health + rhs.Health,
			lhs.Power + rhs.Power
			);
	}

	public static Stats operator *(Stats lhs, Stats rhs) {
		return new Stats(
			(lhs.Health * rhs.Health) / 100,
			(lhs.Power * rhs.Power) / 100
			);
	}

	#endregion

}
