﻿using PoiDefinitions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

namespace Pois {

	public class RecruitmentPoi : Poi {

		public delegate void VisitorHandler(Visitor visitor);
		public event VisitorHandler OnVisitorArrived;
		public event VisitorHandler OnVisitorDeparted;

		public RecruitmentDefinition RecruitmentDefinition { get { return Definition as RecruitmentDefinition; } }

		public struct Visitor {
			public Hero hero;
			public int arrivalTime;
			public int departureTime;

			public Visitor(Hero hero, int arrivalTime, int departureTime) {
				this.hero = hero;
				this.arrivalTime = arrivalTime;
				this.departureTime = departureTime;
			}

			public static Visitor FromJson(JsonData root, WorldState world) {
				int arrivalTime = (int)root["arrivalTime"];
				int departureTime = (int)root["departureTime"];

				Hero hero = null;
				if(root.Keys.Contains("hero")) {
					hero = Hero.FromJson(root["hero"], world);
				}

				return new Visitor(hero, arrivalTime, departureTime);
			}

			public JsonData ToJson() {
				return new JsonData() {
					["arrivalTime"] = arrivalTime,
					["departureTime"] = departureTime,
					["hero"] = hero.ToJson(),
				};
			}
		}

		public List<Visitor> Visitors { get { return visitors; } }

		public void RemoveVisitor(Visitor visitor) {
			visitors.Remove(visitor);
			OnVisitorDeparted?.Invoke(visitor);
		}

		public override bool IsHeroAssigned(Hero hero) {
			return false;
		}

		public override void Tick(WorldState world) {
			// Deal with our visitors first.
			for(int i = visitors.Count - 1; i >= 0; --i) {
				Visitor visitor = visitors[i];
				if(world.CurrentTime >= visitor.departureTime) {
					// Good bye!
					visitors.RemoveAt(i);
					OnVisitorDeparted?.Invoke(visitor);
				}
			}

			if(world.CurrentTime >= nextVisitorTime) {
				if(visitors.Count < RecruitmentDefinition.MaxVisitors) {
					// New visitor!
					int arrivalTime = world.CurrentTime;
					int duration = Random.Range(RecruitmentDefinition.MinHeroVisitTime, RecruitmentDefinition.MaxHeroVisitTime + 1);
					int departureTime = arrivalTime + duration;

					// Generate a hero!
					Hero hero = HeroGenerator.Generate(world, HeroOwner.NPC);

					Visitor visitor = new Visitor(hero, arrivalTime, departureTime);
					visitors.Add(visitor);
					OnVisitorArrived?.Invoke(visitor);
				}

				nextVisitorTime = world.CurrentTime + Random.Range(RecruitmentDefinition.MinNewVisitorTime, RecruitmentDefinition.MaxNewVisitorTime + 1);
			}
		}

		/************************************************************************/
		#region Construction

		public RecruitmentPoi(PoiDefinition definition, JsonData root, WorldState world) : base(definition) {
			visitors = new List<Visitor>();
			if(root != null && root.Keys.Contains("visitors")) {
				JsonData element = root["visitors"];
				for(int i = 0; i < element.Count; ++i) {
					Visitor visitor = Visitor.FromJson(element[i], world);
					// Ditch it if it's old data.
					if(visitor.hero == null) {
						continue;
					}

					visitors.Add(visitor);
				}
			}

			nextVisitorTime = world.CurrentTime;
			if(root != null && root.Keys.Contains("nextVisitorTime")) {
				nextVisitorTime = (int)root["nextVisitorTime"];
			}
		}

		#endregion

		/************************************************************************/
		#region Serialization

		protected override void FillSaveData(JsonData root) {
			JsonData visitorsElement = new JsonData();
			visitorsElement.SetJsonType(JsonType.Array);
			for(int i = 0; i < visitors.Count; ++i) {
				visitorsElement.Add(visitors[i].ToJson());
			}
			root["visitors"] = visitorsElement;

			root["nextVisitorTime"] = nextVisitorTime;
		}

		#endregion

		/************************************************************************/
		#region Privates

		private List<Visitor> visitors;
		private int nextVisitorTime;

		#endregion

	}

}
