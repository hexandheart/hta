﻿using PoiDefinitions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System;

namespace Pois {

	public class RecoveryPoi : Poi {

		public static event Hero.HeroPoiHandler OnHeroRecovered;

		public RecoveryDefinition RecoveryDefinition { get { return Definition as RecoveryDefinition; } }

		public struct Visitor {
			public Hero hero;
			public int arrivalTime;

			public Visitor(Hero hero, int arrivalTime) {
				this.hero = hero;
				this.arrivalTime = arrivalTime;
			}

			public static Visitor FromJson(JsonData root, WorldState world) {
				Hero hero = world.GetHero((string)root["heroId"]);
				int arrivalTime = (int)root["arrivalTime"];

				return new Visitor(hero, arrivalTime);
			}

			public JsonData ToJson() {
				return new JsonData() {
					["heroId"] = hero.ID,
					["arrivalTime"] = arrivalTime,
				};
			}
		}

		public Hero GetHeroAtIndex(int index) {
			return visitors[index].hero;
		}

		public void SetHeroAtIndex(Hero hero, int index, WorldState world) {
			visitors[index] = new Visitor(hero, world.CurrentTime);
		}

		public override bool IsHeroAssigned(Hero hero) {
			for(int i = 0; i < visitors.Length; ++i) {
				if(visitors[i].hero == hero) {
					return true;
				}
			}

			return false;
		}

		public override void Tick(WorldState world) {
			for(int i = 0; i < visitors.Length; ++i) {
				if(visitors[i].hero == null) {
					continue;
				}
				int visitTime = world.CurrentTime - visitors[i].arrivalTime + 1;
				if(visitTime % RecoveryDefinition.TimePerRecovery == 0) {
					Hero visitor = visitors[i].hero;
					visitor.Stats.Health += (visitor.BaseStats.Health * RecoveryDefinition.Recovery) / 100;
					if(visitor.Stats.Health >= visitor.BaseStats.Health) {
						visitor.Stats.Health = visitor.BaseStats.Health;
						// Get out!
						SetHeroAtIndex(null, i, world);

						OnHeroRecovered?.Invoke(visitor, this);
					}
				}
			}
		}

		/************************************************************************/
		#region Construction

		public RecoveryPoi(PoiDefinition definition, JsonData root, WorldState world) : base(definition) {
			visitors = new Visitor[RecoveryDefinition.NumSlots];
			if(root != null && root.Keys.Contains("visitors")) {
				JsonData element = root["visitors"];
				for(int i = 0; i < visitors.Length; ++i) {
					if(i >= element.Count) {
						Debug.LogError("Too many visitors for this recovery POI: " + definition.Name);
						break;
					}
					if(element[i] == null) {
						visitors[i] = new Visitor();
					}
					else {
						visitors[i] = Visitor.FromJson(element[i], world);
					}
				}
			}
		}

		#endregion

		/************************************************************************/
		#region Serialization

		protected override void FillSaveData(JsonData root) {
			JsonData visitorsElement = new JsonData();
			visitorsElement.SetJsonType(JsonType.Array);
			for(int i = 0; i < visitors.Length; ++i) {
				if(visitors[i].hero == null) {
					visitorsElement.Add(null);
				}
				else {
					visitorsElement.Add(visitors[i].ToJson());
				}
			}

			root["visitors"] = visitorsElement;
		}

		#endregion

		/************************************************************************/
		#region Privates

		private Visitor[] visitors;

		#endregion

	}

}
