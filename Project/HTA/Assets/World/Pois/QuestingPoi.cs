﻿using PoiDefinitions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

namespace Pois {

	public class QuestingPoi : Poi {

		public QuestingDefinition QuestingDefinition { get { return Definition as QuestingDefinition; } }

		public List<Quest> Quests { get { return quests; } }

		public override bool IsHeroAssigned(Hero hero) {
			foreach(var quest in quests) {
				for(int i = 0; i < quest.GetNumSlots(); ++i) {
					if(quest.GetHeroInSlot(i) == hero) {
						return true;
					}
				}
			}

			return false;
		}

		public override void Tick(WorldState world) {
			// Update our quests.
			for(int i = quests.Count - 1; i >= 0; --i) {
				Quest quest = quests[i];
				
				if(quest.Result != ActivityResult.Incomplete || // Completed quests need to be pruned.
					!quest.IsActive() && // Active quests get ticked in WorldState and shouldn't be pruned.
					quest.DespawnTime > quest.SpawnTime && // Non-expiring quests don't get pruned.
					world.CurrentTime > quest.DespawnTime) {
					// Prune!
					quest.TriggerExpired();
					quests.RemoveAt(i);
				}
			}

			while(quests.Count < QuestingDefinition.MaxQuests) {
				Quest quest = SpawnQuest(world);
				quests.Add(quest);
				quest.TriggerSpawned();
			}
		}

		/************************************************************************/
		#region Construction

		public QuestingPoi(PoiDefinition definition, JsonData root, WorldState world) : base(definition) {
			quests = new List<Quest>();
			if(root != null && root.Keys.Contains(Quest.GroupName)) {
				JsonData element = root[Quest.GroupName];
				for(int i = 0; i < element.Count; ++i) {
					quests.Add(Quest.FromJson(element[i], world));
				}
			}
		}

		#endregion

		/************************************************************************/
		#region Serialization

		protected override void FillSaveData(JsonData root) {
			JsonData questsElement = new JsonData();
			questsElement.SetJsonType(JsonType.Array);
			for(int i = 0; i < quests.Count; ++i) {
				questsElement.Add(quests[i].ToJson());
			}
			root[Quest.GroupName] = questsElement;
		}

		#endregion

		/************************************************************************/
		#region Privates

		private Quest SpawnQuest(WorldState world) {
			QuestDefinition definition = QuestingDefinition.Quests[UnityEngine.Random.Range(0, QuestingDefinition.Quests.Count)];

			string id = Guid.NewGuid().ToString();
			int spawnTime = world.CurrentTime;
			int despawnTime = spawnTime + 3 * WorldState.TicksPerDay;
			Hero[] slots = new Hero[definition.HeroSlots];
			ActivityTemplateDefinition template = definition.Template;

			Quest quest = new Quest(
				id,
				definition.Name, definition.Info,
				definition.Reward.Gold,
				spawnTime, despawnTime,
				ActivityResult.Incomplete,
				slots, template);
			return quest;
		}

		private List<Quest> quests;

		#endregion

	}

}
