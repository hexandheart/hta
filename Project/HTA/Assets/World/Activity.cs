﻿using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Activities;

public enum ActivityType {
	Quest,
}

public enum ActivityResult {
	Incomplete,
	Failure,
	Success,
}

public abstract class Activity {

	public delegate void ActivityHandler(Activity activity);
	public static ActivityHandler OnStarted;
	public ActivityHandler OnCompleted;

	public string ID { get; private set; }
	public abstract ActivityType Type { get; }
	public abstract string Name { get; }

	public int StartTime { get; private set; }

	public int CurrentProgress { get; protected set; }
	public int TargetProgress { get; protected set; }
	public int FinishDuration { get; private set; }

	public int GetNumHeroes() { return heroes.Length; }
	public Hero GetHero(int index) { return heroes[index]; }

	public ActivityEvent GetCurrentEvent() {
		if(currentEventIndex < 0) {
			return null;
		}
		else {
			return activityEvents[currentEventIndex];
		}
	}

	public void SetCurrentEvent(int index) {
		currentEventIndex = index;
	}

	public int GetNumEvents() {
		return activityEvents.Count;
	}

	public ActivityEvent GetEvent(int index) {
		return activityEvents[index];
	}

	public void ResolveEvent() {
		if(currentEventIndex < 0) {
			return;
		}
		ActivityEvent currentEvent = GetCurrentEvent();

		if(currentEvent.Result == ActivityResult.Failure ||
			currentEventIndex == activityEvents.Count - 1) {
			// Don't linger!
			TargetProgress = CurrentProgress + FinishDuration;
		}

		currentEventIndex = -1;
		currentEventTime = 0;
	}

	public ActivityResult GetResult() {
		ActivityResult result = ActivityResult.Success;
		foreach(var ae in activityEvents) {
			if(ae.Result == ActivityResult.Failure) {
				return ActivityResult.Failure;
			}
			else if(ae.Result == ActivityResult.Incomplete) {
				result = ActivityResult.Incomplete;
			}
		}
		return result;
	}

	public void Tick(WorldState world) {// Is there an active event?
		ActivityResult result = GetResult();
		
		// Is the activity still active?
		if(result == ActivityResult.Incomplete) {
			// Is there an event active?
			ActivityEvent currentEvent = GetCurrentEvent();
			if(currentEvent != null) {
				// Autoresolve if the appropriate amount of time has passed.
				if(world.CurrentTime >= currentEventTime + currentEvent.TriggerDuration) {
					currentEvent.AutoResolve(world);
					ResolveEvent();
					Debug.LogFormat("{0}> Auto-resolved event: {1} ({2})", ID.Substring(0, 8), currentEvent.Type, currentEvent.Result);
				}
			}
			else {
				++CurrentProgress;
				// Do we need to trigger an event now?
				for(int i = 0; i < activityEvents.Count; ++i) {
					ActivityEvent e = activityEvents[i];
					if(CurrentProgress >= e.TriggerProgress &&
						e.Result == ActivityResult.Incomplete) {
						Debug.LogFormat("{0}> Triggering event: {1}", ID.Substring(0, 8), e.Type);
						SetCurrentEvent(i);
						currentEventTime = world.CurrentTime;
					}
				}
			}
		}
		// If it's not active, we just pump the activity to completion.
		else {
			++CurrentProgress;
			if(CurrentProgress >= TargetProgress) {
				CurrentProgress = TargetProgress;
				// Finish up!
				OnComplete(world, result);
			}
		}
	}

	public void TriggerStarted() {
		OnStarted?.Invoke(this);
	}

	public void TriggerCompleted() {
		OnCompleted?.Invoke(this);
	}

	public abstract void OnTick(WorldState world);
	public abstract void OnComplete(WorldState world, ActivityResult result);

	/************************************************************************/
	#region Construction

	protected Activity(string id, int startTime, int currentProgress, int targetProgress, int finishDuration, Hero[] heroes, List<ActivityEvent> activityEvents, int currentEventIndex, int currentEventTime) {
		ID = id;
		StartTime = startTime;
		CurrentProgress = currentProgress;
		TargetProgress = targetProgress;
		FinishDuration = finishDuration;
		this.heroes = heroes;
		this.activityEvents = activityEvents;

		foreach(var ae in this.activityEvents) {
			ae.Activity = this;
		}

		this.currentEventIndex = currentEventIndex;
		this.currentEventTime = currentEventTime;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static readonly string GroupName = "activities";

	public static Activity FromJson(JsonData root, WorldState world) {
		string id = (string)root["id"];
		ActivityType type = (ActivityType)System.Enum.Parse(typeof(ActivityType), (string)root["type"], false);

		int startTime = world.CurrentTime;
		if(root.Keys.Contains("startTime")) {
			startTime = (int)root["startTime"];
		}

		int currentProgress = 0;
		int targetProgress = 0;
		int finishDuration = 0;
		if(root.Keys.Contains("progress")) {
			JsonData element = root["progress"];
			currentProgress = (int)element["current"];
			targetProgress = (int)element["target"];
			finishDuration = (int)element["finishDuration"];
		}

		Hero[] heroes = null;
		if(root.Keys.Contains("heroes")) {
			JsonData element = root["heroes"];
			heroes = new Hero[element.Count];
			for(int i = 0; i < element.Count; ++i) {
				if(element[i] == null) {
					heroes[i] = null;
					continue;
				}

				heroes[i] = world.GetHero((string)element[i]);
			}
		}
		else {
			heroes = new Hero[0];
		}

		List<ActivityEvent> activityEvents = null;
		if(root.Keys.Contains("events")) {
			JsonData element = root["events"];
			activityEvents = new List<ActivityEvent>(element.Count);
			for(int i = 0; i < element.Count; ++i) {
				ActivityEvent activityEvent = ActivityEvent.FromJson(element[i], i, world);
				activityEvents.Add(activityEvent);
			}
		}
		else {
			activityEvents = new List<ActivityEvent>();
		}

		int currentEventIndex = -1;
		if(root.Keys.Contains("currentEventIndex")) {
			currentEventIndex = (int)root["currentEventIndex"];
		}

		int currentEventTime = 0;
		if(root.Keys.Contains("currentEventTime")) {
			currentEventTime = (int)root["currentEventTime"];
		}

		switch(type) {
		case ActivityType.Quest:
			return new QuestActivity(id, startTime, currentProgress, targetProgress, finishDuration, heroes, activityEvents, currentEventIndex, currentEventTime, root, world);
		}

		return null;
	}

	public JsonData ToJson() {
		JsonData heroesGroup = new JsonData();
		heroesGroup.SetJsonType(JsonType.Array);
		for(int i = 0; i < heroes.Length; ++i) {
			if(heroes[i] == null) {
				heroesGroup.Add(null);
			}
			else {
				heroesGroup.Add(heroes[i].ID);
			}
		}

		JsonData eventsGroup = new JsonData();
		eventsGroup.SetJsonType(JsonType.Array);
		for(int i = 0; i < activityEvents.Count; ++i) {
			eventsGroup.Add(activityEvents[i].ToJson());
		}

		JsonData root = new JsonData() {
			["id"] = ID,
			["startTime"] = StartTime,
			["type"] = Type.ToString(),
			["progress"] = new JsonData() {
				["current"] = CurrentProgress,
				["target"] = TargetProgress,
				["finishDuration"] = FinishDuration,
			},
			["heroes"] = heroesGroup,
			["events"] = eventsGroup,
			["currentEventIndex"] = currentEventIndex,
			["currentEventTime"] = currentEventTime,
		};

		FillSaveData(root);

		return root;
	}

	protected abstract void FillSaveData(JsonData root);

	#endregion

	/************************************************************************/
	#region Privates

	private Hero[] heroes;
	private List<ActivityEvent> activityEvents;
	private int currentEventIndex;
	private int currentEventTime;

	#endregion

}
