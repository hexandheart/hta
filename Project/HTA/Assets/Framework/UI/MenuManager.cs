﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Events;

public class MenuManager : MonoBehaviour {

	private static MenuManager instance;
	public static MenuManager Instance { get { return instance; } }

	[SerializeField]
	private Menu initialMenu;

	private static Dictionary<System.Type, Menu> menus = new Dictionary<System.Type, Menu>();

	private Menu openMenu;

	public static void RegisterMenu(Menu menu) {
		menus[menu.GetType()] = menu;
	}

	private void Awake() {
		instance = this;
		//TM.LoadLanguage("en_US"); // Loc
	}

	private void Start() {
		OpenMenu(initialMenu);
	}

	public static void OpenMenu<T>(params object[] args) where T : Menu {
		//T menu = FindObjectOfType<T>();
		T menu = (T)menus[typeof(T)];

		instance.OpenMenu(menu, args);
	}

	public static void OpenOverlay<T>(params object[] args) where T : Menu {
		//T menu = FindObjectOfType<T>();
		T menu = (T)menus[typeof(T)];

		instance.OpenOverlay(menu, args);
	}

	public static void CloseOverlay<T>() where T : Menu {
		//T menu = FindObjectOfType<T>();
		T menu = (T)menus[typeof(T)];

		instance.CloseOverlay(menu);
	}

	public void OpenMenu(Menu menu, params object[] args) {
		if(openMenu != null) {
			// Don't do anything if we want to show the already-open menu.
			if(openMenu == menu) {
				return;
			}

			openMenu.Close();
		}

		openMenu = menu;
		if(openMenu == null) {
			return;
		}
		//openMenu.IsOpen = true;
		StartCoroutine(OpenNextMenu(openMenu, args));
	}

	public void OpenOverlay(Menu menu, params object[] args) {
		if(menu == null) {
			return;
		}
		StartCoroutine(OpenNextMenu(menu, args));
	}

	public void CloseOverlay(Menu menu) {
		if(menu == null) {
			return;
		}
		menu.Close();
	}

	public static void CloseOverlays(System.Func<Menu, bool> filter = null) {
		foreach(var menu in menus.Values) {
			if(!menu.IsOverlay ||
				filter != null && !filter(menu)) {
				continue;
			}
			menu.Close();
		}
	}

	private IEnumerator OpenNextMenu(Menu menu, params object[] args) {
		yield return new WaitForEndOfFrame();
		menu.gameObject.SetActive(true);
		menu.Open(args);
	}

}
