﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

[SelectionBase]
public class Menu : MonoBehaviour {

	public virtual bool IsOverlay { get { return false; } }

	public bool IsOpen {
		get {
			return gameObject.activeSelf && animator.GetBool("IsOpen");
		}
		private set {
			if(value == false) {
				canvasGroup.interactable = false;
			}
			animator.SetBool("IsOpen", value);
		}
	}

	private Animator animator;
	private CanvasGroup canvasGroup;

	[SerializeField]
	protected GameObject defaultSelection;

	private void Awake() {
		MenuManager.RegisterMenu(this);

		animator = GetComponent<Animator>();
		canvasGroup = GetComponent<CanvasGroup>();

		if(animator == null || canvasGroup == null) {
			gameObject.SetActive(false);
			return;
		}

		RectTransform rect = GetComponent<RectTransform>();
		rect.anchoredPosition = Vector2.zero;

		OnAwake();
		gameObject.SetActive(false);
	}

	protected virtual void OnAwake() {}

	private void Update() {
		if(animator.GetCurrentAnimatorStateInfo(0).IsName("Open")) {
			canvasGroup.blocksRaycasts = true;
		}
		else {
			canvasGroup.blocksRaycasts = false;
		}

		if(IsOpen) {
			OnUpdate();
		}
	}

	protected virtual void OnUpdate() {}

	public void Open(params object[] args) {
		if(!IsOpen) {
			OnOpen(args);
		}
		IsOpen = true;
	}

	protected virtual void OnOpen(params object[] args) { }

	public void Close() {
		if(IsOpen) {
			IsOpen = false;
			OnClose();
		}
	}

	protected virtual void OnClose() { }

	protected virtual void OnTransitionInComplete() {
		canvasGroup.interactable = true;
		if(defaultSelection != null) {
			EventSystem.current.SetSelectedGameObject(defaultSelection);
		}
	}

	protected virtual void OnTransitionOutComplete() {
		if(IsOpen) {
			return;
		}
		canvasGroup.interactable = false;
		gameObject.SetActive(false);
	}

}
