﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Events;
using UnityEngine.SceneManagement;

public enum TransitionState {
	Entering,
	Active,
	Exiting,
}

/// <summary>
/// <para>Support for transitioning between game states.</para>
/// </summary>
public class GameStateManager : MonoBehaviour {

	public delegate void GameStateHandler(string state, TransitionState transition);
	public static GameStateHandler OnGameStateChanged;

	/// <summary>
	/// <para>The transition which is underway will not complete until the supplied coroutine
	/// has finished.</para>
	/// </summary>
	/// <param name="coroutine">Coroutine to wait for.</param>
	public static void RegisterTransitionCoroutine(IEnumerator coroutine) {
		++instance.waitCount;
		// TODO: Ideally, the coroutine should be able to signal an abort in the transition.
		instance.StartCoroutine(instance.Wrapper(coroutine));
	}

	/// <summary>
	/// <para>Request a transition to the specified state. Events will be triggered as the
	/// transition proceeds, in this order:</para>
	/// <para>1. Current State - Exiting</para>
	/// <para>2. New State - Entering</para>
	/// <para>3. New State - Active</para>
	/// <para>At any point before New State - Active, GameStateManager.RegisterTransitionCoroutine()
	/// can be called to force the transition to wait until the specified coroutine is complete.</para>
	/// </summary>
	/// <param name="newState"></param>
	public static void RequestState(string newState) {
		instance.transitionQueue.Enqueue(newState);
	}

	/***********************************************************************************************/

	private static GameStateManager instance;

	[SerializeField]
	private string initialState = "MainMenu";

	private string gameState = "Boot";
	private int waitCount = 0;
	private Queue<string> transitionQueue = new Queue<string>();

	/// <summary>
	/// Kick off the initial transition on app start.
	/// </summary>
	/// <returns></returns>
	private IEnumerator Start() {
		instance = this;
		yield return null;
		SceneManager.LoadSceneAsync("Empty", LoadSceneMode.Single);
		StartCoroutine(TransitionPump());
		RequestState(initialState);
	}

	/// <summary>
	/// Internal wrapper for counting coroutine completion.
	/// </summary>
	/// <param name="coroutine"></param>
	/// <returns></returns>
	private IEnumerator Wrapper(IEnumerator coroutine) {
		yield return StartCoroutine(coroutine);
		OnCoroutineComplete();
	}

	/// <summary>
	/// Internal call to decrement counter on coroutine completion.
	/// </summary>
	private void OnCoroutineComplete() {
		--waitCount;
	}

	private IEnumerator TransitionPump() {
		while(true) {
			if(transitionQueue.Count == 0) {
				yield return null;
				continue;
			}

			// There is a transition!
			MenuManager.OpenOverlay<LoadingOverlay>();
			MenuManager.CloseOverlays(m => m.GetType() != typeof(LoadingOverlay));
			yield return new WaitForSecondsRealtime(0.5f);

			string nextState = transitionQueue.Dequeue();
			yield return StartCoroutine(TransitionToState(nextState));

			if(transitionQueue.Count == 0) {
				MenuManager.CloseOverlay<LoadingOverlay>();
				yield return new WaitForSecondsRealtime(0.5f);
			}
		}
	}

	/// <summary>
	/// <para>Does the work of transitioning to a new state.</para>
	/// 
	/// <para>Automatically shows the LoadingOverlay for the duration of the transition, and
	/// automatically loads an appropriate menu once the transition is complete.</para>
	/// </summary>
	/// <param name="newState"></param>
	/// <returns></returns>
	private IEnumerator TransitionToState(string newState) {
		yield return StartCoroutine(WaitForTransition(gameState, TransitionState.Exiting));

		if(waitCount != 0) {
			Debug.LogWarningFormat(this, "GameStateManager: Requesting transition to state {0}, but waiting on {1} coroutine(s).", newState, waitCount);
			while(waitCount > 0) {
				yield return null;
			}
		}

		gameState = newState;
		yield return StartCoroutine(WaitForTransition(gameState, TransitionState.Entering));

		OnGameStateChanged?.Invoke(gameState, TransitionState.Active);
	}

	/// <summary>
	/// <para>TransitionToState() uses this coroutine to wait for registered coroutines.</para>
	/// </summary>
	/// <param name="state"></param>
	/// <param name="transition"></param>
	/// <returns></returns>
	private IEnumerator WaitForTransition(string state, TransitionState transition) {
		OnGameStateChanged?.Invoke(state, transition);

		// Give listeners a chance to react.
		yield return null;
		// Wait until provided transition coroutines are complete.
		while(waitCount > 0) {
			yield return null;
		}
	}

}