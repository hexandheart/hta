﻿using UnityEngine;
using System.Collections;
using System.IO;

// originally found in: http://forum.unity3d.com/threads/35617-TextManager-Localization-Script

/// <summary>
/// TM
///
/// Reads PO files in the Assets\Resources\Languages directory into a Hashtable.
/// Look for "PO editors" as that's a standard for translating software.
///
/// Example:
///
/// load the language file:
///   TM.LoadLanguage("helptext-pt-br");
///
/// which has to contain at least two lines as such:
///   msgid "HELLO WORLD"
///   msgstr "OLA MUNDO"
///
/// then we can retrieve the text by calling:
///   TM._("HELLO WORLD");
/// </summary>
public class TM : MonoBehaviour {

	private static TM instance;
	private static Hashtable textTable;
	private TM() { }

	private static TM Instance {
		get {
			if(instance == null) {
				TM oldManager = Component.FindObjectOfType(typeof(TM)) as TM;
				if(oldManager != null) {
					instance = oldManager;
					DontDestroyOnLoad(instance.gameObject);
				}
				else {
					GameObject go = new GameObject("_TextManager");
					DontDestroyOnLoad(go);
					instance = go.AddComponent<TM>();
				}
			}
			return instance;
		}
	}

	public static TM GetInstance() {
		return Instance;
	}

	public static bool LoadLanguage(string filename) {
		return LoadLanguage(filename, false);
	}

	public static bool LoadLanguage(string filename, bool clear) {
		GetInstance();

		if(filename == null) {
			Debug.Log("[TM] loading default language.");
			textTable = null; // reset to default
			return false; // this means: call LoadLanguage with null to reset to default
		}

		string fullpath = "Languages/" + filename + ".po"; // the file is actually ".txt" in the end

		TextAsset textAsset = (TextAsset)Resources.Load(fullpath, typeof(TextAsset));
		if(textAsset == null) {
			Debug.LogError("[TM] " + fullpath + " file not found.");
			return false;
		}

		Debug.Log("[TM] loading: " + fullpath);

		if(textTable == null) {
			textTable = new Hashtable();
		}

		if(clear) {
			textTable.Clear();
		}

		StringReader reader = new StringReader(textAsset.text);
		string key = null;
		string val = null;
		string line;
		while((line = reader.ReadLine()) != null) {
			if(line.StartsWith("msgid \"")) {
				key = line.Substring(7, line.Length - 8);
			}
			else if(line.StartsWith("msgstr \"")) {
				val = line.Substring(8, line.Length - 9);

				if(key == null) {
					Debug.LogError("Error in " + fullpath + ": string with no id");
				}
				else if(textTable.ContainsKey(key)) {
					Debug.LogWarning("Warning in " + fullpath + ": duplicate key " + key);
				}
				else {
					if(!key.Equals("") && !val.Equals("")) {
						textTable.Add(key, val);
					}
				}
				key = val = null;
			}
		}

		reader.Close();

		return true;
	}


	public static string _(string key) {
		if(key != null && textTable != null) {
			if(textTable.ContainsKey(key)) {
				string result = (string)textTable[key];
				if(result.Length > 0) {
					return result;
				}
			}
		}
		return "*" + key;
	}
}