﻿using System.Collections.Generic;
using System.Linq;

public static class Enums {

	public static IEnumerable<T> Get<T>() {
		return System.Enum.GetValues(typeof(T)).Cast<T>();
	}

	public static System.Array AsArray<T>() {
		return System.Enum.GetValues(typeof(T));
	}

	public static int Length<T>() {
		return System.Enum.GetValues(typeof(T)).Length;
	}

}