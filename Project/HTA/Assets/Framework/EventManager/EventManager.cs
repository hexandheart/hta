﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Events {
	public sealed class EventManager : MonoBehaviour {
		private static EventManager instance;

		private Dictionary<Type, List<Action<IEvent>>> eventListenerMap;
		private LinkedList<IEvent> events;

		private static EventManager Instance {
			get {
				if(null == instance) {
					GameObject go = new GameObject("EventManager");
					go.AddComponent<EventManager>();
				}
				return instance;
			}
		}

		private static bool wasDestroyed = false;

		private void Awake() {
			instance = this;
			DontDestroyOnLoad(this);
			eventListenerMap = new Dictionary<Type, List<Action<IEvent>>>();
			events = new LinkedList<IEvent>();
		}

		private void OnDestroy() {
			wasDestroyed = true;
			instance = null;
		}

		/// <summary>
		/// Registers the event listener for the given event.
		/// </summary>
		/// <returns>
		/// True if the event listener was added successfully,
		/// False otherwise.
		/// </returns>
		/// <param name='listener'>
		/// The listener to add.
		/// </param>
		/// <typeparam name='T'>
		/// The type of event to listen for.
		/// </typeparam>
		public static bool AddListener<T>(Action<IEvent> listener)
			where T : IEvent {
			if(wasDestroyed) {
				return false;
			}

			Type eventType = typeof(T);
			return AddListener(eventType, listener);
		}

		public static bool AddListener(Type eventType, Action<IEvent> listener) {
			if(wasDestroyed) {
				return false;
			}

			bool eventListenerWasAdded = false;

			// Check if we've already registered this event listener, so that we can 
			// avoid registering for it twice. 
			if(Instance.eventListenerMap.ContainsKey(eventType)) {
				List<Action<IEvent>> eventListenerList = Instance.eventListenerMap[eventType];
				int findIndex = eventListenerList.FindIndex((obj) => obj == listener);
				if(findIndex >= 0) {
					// This listener is already listening.
					eventListenerWasAdded = false;
				}
				else {
					eventListenerList.Add(listener);
					eventListenerWasAdded = true;
				}
			}
			else {
				// This event type doesn't have any listeners yet. 
				// Let's get it set up and add the listener.
				List<Action<IEvent>> eventListener = new List<Action<IEvent>>();
				eventListener.Add(listener);
				Instance.eventListenerMap.Add(eventType, eventListener);
				eventListenerWasAdded = true;
			}

			return eventListenerWasAdded;
		}

		/// <summary>
		/// Removes the listener so that events of the given type no longer call it.
		/// </summary>
		/// <returns>
		/// True if the listener was removed successfully,
		/// False otherwise.
		/// </returns>
		/// <param name='listener'>
		/// The listener to remove.
		/// </param>
		/// <typeparam name='T'>
		/// The type of event to remove.
		/// </typeparam>
		public static bool RemoveListener<T>(Action<IEvent> listener)
			where T : IEvent {
			if(wasDestroyed) {
				return false;
			}

			Type eventType = typeof(T);
			return RemoveListener(eventType, listener);
		}

		public static bool RemoveListener(Type eventType, Action<IEvent> listener) {
			if(wasDestroyed) {
				return false;
			}

			bool eventListenerWasRemoved = false;

			if(Instance.eventListenerMap.ContainsKey(eventType)) {
				List<Action<IEvent>> eventListenerList = Instance.eventListenerMap[eventType];
				int findIndex = eventListenerList.FindIndex((obj) => obj == listener);
				if(findIndex >= 0) {
					eventListenerList.RemoveAt(findIndex);
					eventListenerWasRemoved = true;
				}
			}

			return eventListenerWasRemoved;
		}

		/// <summary>
		/// Queues the event, to be fired as soon as possible (not necessarily within the current frame). 
		/// If you must fire the event in the current frame, consider using Trigger instead.
		/// </summary>
		/// <returns>
		/// True if the event was queued successfully, 
		/// False otherwise.
		/// </returns>
		/// <param name='eventData'>
		/// The event to queue.
		/// </param>
		public static bool Queue(IEvent eventData) {
			if(wasDestroyed) {
				return false;
			}

			// We'll only add the event if there is actually a listener for it.
			if(Instance.eventListenerMap.ContainsKey(eventData.GetType())) {
				Instance.events.AddLast(new LinkedListNode<IEvent>(eventData));
				return true;
			}
			else {
				// No listeners.
				return false;
			}
		}

		/// <summary>
		/// Immediately triggers the given event, bypassing
		/// any waiting period or queue.
		/// </summary>
		/// <returns>
		/// True if the event was triggered successfully, 
		/// False otherwise.
		/// </returns>
		/// <param name='eventData'>
		/// The event to trigger.
		/// </param>
		public static bool Trigger(IEvent eventData) {
			if(wasDestroyed) {
				return false;
			}

			// We'll only trigger the event if there is actually a listener for it.
			Type eventType = eventData.GetType();
			if(Instance.eventListenerMap.ContainsKey(eventType)) {
				List<Action<IEvent>> eventListenerList = Instance.eventListenerMap[eventType];
				foreach(Action<IEvent> eventListener in eventListenerList) {
					eventListener(eventData);
				}
				return true;
			}
			else {
				// No listeners.
				return false;
			}

		}

		/// <summary>
		/// Removes all occurrences of the given event type.
		/// </summary>
		/// <returns>
		/// True if the event was removed successfully,
		/// False otherwise.
		/// </returns>
		/// <typeparam name='T'>
		/// The type of event to remove.
		/// </typeparam>
		public static bool Remove<T>()
			where T : IEvent {
			if(wasDestroyed) {
				return false;
			}

			bool eventRemoved = false;

			Type eventType = typeof(T);
			if(Instance.eventListenerMap.ContainsKey(eventType)) {
				LinkedListNode<IEvent> node = Instance.events.First;
				while(node != null) {
					var next = node.Next;
					if(node.Value.GetType() == eventType) {
						Instance.events.Remove(node);
						eventRemoved = true;
					}
					node = next;
				}
			}

			return eventRemoved;
		}

		private void Update() {
			// Only process the events already placed in the queue.
			int maxEventsToProcess = events.Count;
			int processedEventCount = 0;
			float startTime = Time.realtimeSinceStartup;
			LinkedListNode<IEvent> node = events.First;
			while(processedEventCount < maxEventsToProcess && node != null && events.Count > 0) {
				++processedEventCount;
				IEvent eventData = node.Value;
				var next = node.Next;

				events.Remove(node);

				// Call all the event listeners that are registered for this event.
				List<Action<IEvent>> eventListenerList = eventListenerMap[eventData.GetType()];
				foreach(Action<IEvent> eventListener in eventListenerList) {
					eventListener(eventData);
				}

				if(next != null && next.List == events) {
					node = next;
				}
				else {
					node = events.First;
				}

				// If we've taken too long, suspend processing of events until the next Update.
				float endTime = Time.realtimeSinceStartup - startTime;
				if(endTime > Time.deltaTime) {
					Debug.LogWarning("EventManager Update is taking too long: " + endTime);
					break;
				}
			}
		}
	}
}