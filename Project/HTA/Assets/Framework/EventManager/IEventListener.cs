﻿namespace Events {

	public interface IEventListener {

		void AddListeners();
		void RemoveListeners();

	}

}