﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class MantleDefinition {

	public string ID { get; private set; }
	public MantleType Type { get; private set; }
	public string Name { get; private set; }

	public string CoreSkillID { get; private set; }
	public int HireCostGold { get; private set; }
	public MantleHireSkill[] HireSkills { get; private set; }

	public Stats BaseStats { get; private set; }

	public SkillRating GetRatingForSkill(SkillDefinition skill) {
		if(skillRatings.ContainsKey(skill.ID)) {
			return skillRatings[skill.ID];
		}
		else {
			return skill.BaselingRating;
		}
	}

	/************************************************************************/
	#region Construction

	private MantleDefinition(
			string id, MantleType type,
			string name,
			string coreSkillId,
			int hireCostGold, MantleHireSkill[] hireSkills,
			Stats baseStats,
			Dictionary<string, SkillRating> skillRatings) {
		ID = id;
		Type = type;
		Name = name;
		CoreSkillID = coreSkillId;
		HireCostGold = hireCostGold;
		HireSkills = hireSkills;
		BaseStats = baseStats;
		this.skillRatings = skillRatings;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static MantleDefinition FromJson(JsonData root) {
		string id = (string)root["id"];

		MantleType type = MantleType.Race;
		if(root.Keys.Contains("type")) {
			type = (MantleType)System.Enum.Parse(typeof(MantleType), (string)root["type"], true);
		}

		string name = id;
		if(root.Keys.Contains("name")) {
			name = (string)root["name"];
		}

		string coreSkillId = null;
		if(root.Keys.Contains("coreSkill")) {
			coreSkillId = (string)root["coreSkill"];
		}

		int hireCostGold = 0;
		MantleHireSkill[] hireSkills = new MantleHireSkill[0];
		if(root.Keys.Contains("generation")) {
			JsonData element = root["generation"];
			if(element.Keys.Contains("goldCost")) {
				hireCostGold = (int)element["goldCost"];
			}
			if(element.Keys.Contains("skills")) {
				JsonData skillsGroup = element["skills"];
				hireSkills = new MantleHireSkill[skillsGroup.Count];
				for(int i = 0; i < hireSkills.Length; ++i) {
					hireSkills[i] = MantleHireSkill.FromJson(skillsGroup[i]);
				}
			}
		}

		Stats baseStats = null;
		if(root.Keys.Contains("baseStats")) {
			baseStats = Stats.FromJson(root["baseStats"]);
		}
		else {
			baseStats = Stats.Default;
		}

		Dictionary<string, SkillRating> skillRatings = new Dictionary<string, SkillRating>();
		if(root.Keys.Contains("skillRatings")) {
			JsonData element = root["skillRatings"];
			for(int i = 0; i < element.Count; ++i) {
				string skillId = (string)element[i]["id"];
				SkillRating rating = (SkillRating)System.Enum.Parse(typeof(SkillRating), (string)element[i]["rating"], true);
				skillRatings[skillId] = rating;
			}
		}

		return new MantleDefinition(id, type, name, coreSkillId, hireCostGold, hireSkills, baseStats, skillRatings);
	}

	#endregion

	/************************************************************************/
	#region MantleHireSkill

	public struct MantleHireSkill {

		public string ID { get; private set; }
		public int XPMin { get; private set; }
		public int XPMax { get; private set; }

		public static MantleHireSkill FromJson(JsonData root) {
			return new MantleHireSkill() {
				ID = (string)root["id"],
				XPMin = (int)root["xpMin"],
				XPMax = (int)root["xpMax"],
			};
		}

	}

	#endregion

	/************************************************************************/
	#region MantleSkillRating

	public struct MantleSkillRating {

		public string ID { get; private set; }
		public SkillRating Rating { get; private set; }

		public static MantleSkillRating FromJson(JsonData root) {
			return new MantleSkillRating() {
				ID = (string)root["id"],
				Rating = (SkillRating)System.Enum.Parse(typeof(SkillRating), (string)root["rating"], true),
			};
		}

	}

	#endregion

	/************************************************************************/
	#region Privates

	private Dictionary<string, SkillRating> skillRatings;

	#endregion

}
