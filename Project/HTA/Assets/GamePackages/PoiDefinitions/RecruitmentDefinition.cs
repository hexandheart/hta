﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

namespace PoiDefinitions {

	public class RecruitmentDefinition : PoiDefinition {

		public int MinNewVisitorTime { get; private set; }
		public int MaxNewVisitorTime { get; private set; }
		public int MinHeroVisitTime { get; private set; }
		public int MaxHeroVisitTime { get; private set; }
		public int MaxVisitors { get; private set; }

		/************************************************************************/
		#region Construction

		public RecruitmentDefinition(string id, string name, PoiType type, JsonData root) : base(id, name, type) {
			if(!root.Keys.Contains("data")) {
				Debug.LogError("Poi Recruitment definition doesn't contain a 'data' element.");
				return;
			}
			JsonData dataRoot = root["data"];

			MinNewVisitorTime = 1;
			MaxNewVisitorTime = WorldState.TicksPerDay;
			if(dataRoot.Keys.Contains("newVisitorTime")) {
				JsonData element = dataRoot["newVisitorTime"];
				MinNewVisitorTime = (int)element["min"];
				MaxNewVisitorTime = (int)element["max"];
			}

			MinHeroVisitTime = WorldState.TicksPerDay;
			MaxHeroVisitTime = WorldState.TicksPerDay;
			if(dataRoot.Keys.Contains("heroVisitTime")) {
				JsonData element = dataRoot["heroVisitTime"];
				MinHeroVisitTime = (int)element["min"];
				MinHeroVisitTime = (int)element["max"];
			}

			MaxVisitors = 1;
			if(dataRoot.Keys.Contains("maxVisitors")) {
				MaxVisitors = (int)dataRoot["maxVisitors"];
			}
		}

		#endregion

	}

}
