﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

namespace PoiDefinitions {

	public class RecoveryDefinition : PoiDefinition {

		public int NumSlots { get; private set; }
		public int TimePerRecovery { get; private set; }
		public int Recovery { get; private set; }
		public int GoldCost { get; private set; }

		/************************************************************************/
		#region Construction

		public RecoveryDefinition(string id, string name, PoiType type, JsonData root) : base(id, name, type) {
			if(!root.Keys.Contains("data")) {
				Debug.LogError("Poi Recovery definition doesn't contain a 'data' element.");
				return;
			}
			JsonData dataRoot = root["data"];

			NumSlots = 1;
			if(dataRoot.Keys.Contains("numSlots")) {
				NumSlots = (int)dataRoot["numSlots"];
			}

			TimePerRecovery = 1;
			if(dataRoot.Keys.Contains("timePerRecovery")) {
				TimePerRecovery = (int)dataRoot["timePerRecovery"];
			}

			Recovery = 100;
			if(dataRoot.Keys.Contains("recovery")) {
				Recovery = (int)dataRoot["recovery"];
			}

			GoldCost = 0;
			if(dataRoot.Keys.Contains("cost")) {
				JsonData element = dataRoot["cost"];
				if(element.Keys.Contains("gold")) {
					GoldCost = (int)element["gold"];
				}
			}
		}

		#endregion

	}

}
