﻿using System.Collections;
using System.Collections.Generic;
using LitJson;

namespace PoiDefinitions {

	public class QuestingDefinition : PoiDefinition {

		public int MaxQuests { get; private set; }
		public List<QuestDefinition> Quests { get { return quests; } }

		/************************************************************************/
		#region Construction

		public QuestingDefinition(string id, string name, PoiType type, JsonData root) : base(id, name, type) {
			MaxQuests = 0;
			quests = new List<QuestDefinition>();
			if(root.Keys.Contains("questing")) {
				JsonData element = root["questing"];
				if(element.Keys.Contains("maxQuests")) {
					MaxQuests = (int)element["maxQuests"];
				}

				if(element.Keys.Contains(QuestDefinition.GroupName)) {
					JsonData questElement = element[QuestDefinition.GroupName];
					for(int i = 0; i < questElement.Count; ++i) {
						quests.Add(QuestDefinition.FromJson(questElement[i]));
					}
				}
			}
		}

		#endregion

		/************************************************************************/
		#region Private

		private List<QuestDefinition> quests;

		#endregion

	}

}
