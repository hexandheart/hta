﻿public enum SkillRating {
	NA,
	E,
	D,
	C,
	B,
	A,
	S,
}