﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class ActivityTemplateDefinition {

	public static readonly ActivityTemplateDefinition Default = new ActivityTemplateDefinition(10, new List<ActivityEventDefinition>());

	public int FinishDuration { get; private set; }
	public List<ActivityEventDefinition> ActivityEvents { get { return activityEvents; } }

	/************************************************************************/
	#region Construction

	private ActivityTemplateDefinition(int finishDuration, List<ActivityEventDefinition> activityEvents) {
		FinishDuration = finishDuration;
		this.activityEvents = activityEvents;
	}

	public ActivityTemplateDefinition(ActivityTemplateDefinition o) {
		FinishDuration = o.FinishDuration;

		activityEvents = new List<ActivityEventDefinition>(o.activityEvents.Count);
		foreach(var qe in o.activityEvents) {
			activityEvents.Add(new ActivityEventDefinition(qe));
		}
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static readonly string ElementName = "template";

	public static ActivityTemplateDefinition FromJson(JsonData root) {
		int finishDuration = 10;
		if(root.Keys.Contains("finishDuration")) {
			finishDuration = (int)root["finishDuration"];
		}

		List<ActivityEventDefinition> activityEvents = new List<ActivityEventDefinition>();
		if(root.Keys.Contains("events")) {
			JsonData element = root["events"];
			for(int i = 0; i < element.Count; ++i) {
				activityEvents.Add(ActivityEventDefinition.FromJson(element[i], i));
			}
		}

		return new ActivityTemplateDefinition(finishDuration, activityEvents);
	}

	public JsonData ToJson() {
		JsonData eventsGroup = new JsonData();
		eventsGroup.SetJsonType(JsonType.Array);
		for(int i = 0; i < activityEvents.Count; ++i) {
			eventsGroup.Add(activityEvents[i].ToJson());
		}

		return new JsonData() {
			["finishDuration"] = FinishDuration,
			["events"] = eventsGroup,
		};
	}

	#endregion

	/************************************************************************/
	#region Serialization

	private List<ActivityEventDefinition> activityEvents;

	#endregion

}
