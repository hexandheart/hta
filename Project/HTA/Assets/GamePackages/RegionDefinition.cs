﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class RegionDefinition {

	public string ID { get; private set; }
	public string Name { get; private set; }

	public List<PoiDefinition> Pois { get { return pois; } }

	public bool IsHome { get { return ID == "home"; } }

	/************************************************************************/
	#region Construction

	private RegionDefinition(string id, string name, List<PoiDefinition> pois) {
		ID = id;
		Name = name;
		this.pois = pois;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static RegionDefinition FromJson(JsonData root) {
		string id = (string)root["id"];

		string name = id;
		if(root.Keys.Contains("name")) {
			name = (string)root["name"];
		}

		List<PoiDefinition> pois = new List<PoiDefinition>();
		if(root.Keys.Contains("pois")) {
			JsonData element = root["pois"];
			for(int i = 0; i < element.Count; ++i) {
				PoiDefinition poi = PoiDefinition.FromJson(element[i]);
				pois.Add(poi);
			}
		}

		return new RegionDefinition(id, name, pois);
	}

	#endregion

	/************************************************************************/
	#region Privates

	private List<PoiDefinition> pois;

	#endregion

}
