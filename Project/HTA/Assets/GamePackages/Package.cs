﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class Package {

	public PackageInfo Info { get; private set; }
	public List<RegionDefinition> Regions { get { return regions; } }
	public List<MantleDefinition> Mantles { get { return mantles; } }
	public List<SpellDefinition> Spells { get { return spells; } }
	public List<SkillDefinition> Skills { get { return skills; } }
	public CampaignDefinition Campaign { get; private set; }

	public List<XPTable> XPTables { get { return xpTables; } }

	public RegionDefinition GetRegionDefinition(string id) {
		return regions.Find(r => r.ID == id);
	}

	public PoiDefinition GetPoiDefinition(string id) {
		foreach(var region in regions) {
			PoiDefinition poi = region.Pois.Find(p => p.ID == id);
			if(poi != null) {
				return poi;
			}
		}
		return null;
	}

	public MantleDefinition GetMantleDefinition(string id) {
		return mantles.Find(m => m.ID == id);
	}

	public SpellDefinition GetSpellDefinition(string id) {
		return spells.Find(s => s.ID == id);
	}

	public SkillDefinition GetSkillDefinition(string id) {
		return skills.Find(s => s.ID == id);
	}

	public XPTable GetXPTable(string id) {
		return xpTables.Find(t => t.ID == id);
	}

	/************************************************************************/
	#region Construction

	private Package(
			PackageInfo info,
			List<XPTable> xpTables,
			List<RegionDefinition> regions,
			List<MantleDefinition> mantles,
			List<SpellDefinition> spells,
			List<SkillDefinition> skills,
			CampaignDefinition campaign) {
		Info = info;

		this.xpTables = xpTables;
		this.regions = regions;
		this.mantles = mantles;
		this.spells = spells;
		this.skills = skills;
		Campaign = campaign;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static readonly string ElementName = "package";

	public static Package FromJson(JsonData root, PackageInfo info) {
		List<XPTable> xpTables = new List<XPTable>();
		if(root.Keys.Contains("xpTables")) {
			JsonData element = root["xpTables"];
			for(int i = 0; i < element.Count; ++i) {
				XPTable xpTable = XPTable.FromJson(element[i]);
				xpTables.Add(xpTable);
			}
		}

		List<RegionDefinition> regions = new List<RegionDefinition>();
		if(root.Keys.Contains("regions")) {
			JsonData element = root["regions"];
			for(int i = 0; i < element.Count; ++i) {
				RegionDefinition region = RegionDefinition.FromJson(element[i]);
				regions.Add(region);
			}
		}

		List<SpellDefinition> spells = new List<SpellDefinition>();
		if(root.Keys.Contains("spells")) {
			JsonData element = root["spells"];
			for(int i = 0; i < element.Count; ++i) {
				SpellDefinition spell = SpellDefinition.FromJson(element[i]);
				spells.Add(spell);
			}
		}

		List<SkillDefinition> skills = new List<SkillDefinition>();
		if(root.Keys.Contains("skills")) {
			JsonData element = root["skills"];
			for(int i = 0; i < element.Count; ++i) {
				SkillDefinition skill = SkillDefinition.FromJson(element[i]);
				skills.Add(skill);
			}
		}

		List<MantleDefinition> mantles = new List<MantleDefinition>();
		if(root.Keys.Contains("mantles")) {
			JsonData element = root["mantles"];
			for(int i = 0; i < element.Count; ++i) {
				MantleDefinition mantle = MantleDefinition.FromJson(element[i]);
				mantles.Add(mantle);
			}
		}

		CampaignDefinition campaign = null;
		if(root.Keys.Contains(CampaignDefinition.ElementName)) {
			campaign = CampaignDefinition.FromJson(root[CampaignDefinition.ElementName]);
		}

		return new Package(info, xpTables, regions, mantles, spells, skills, campaign);
	}

	#endregion

	/************************************************************************/
	#region Privates

	private List<RegionDefinition> regions;
	private List<MantleDefinition> mantles;
	private List<SpellDefinition> spells;
	private List<SkillDefinition> skills;

	private List<XPTable> xpTables;

	#endregion

}
