﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class RewardDefinition {

	public static RewardDefinition Default = new RewardDefinition(0);

	public int Gold { get; private set; }

	/************************************************************************/
	#region Construction

	private RewardDefinition(int gold) {
		Gold = gold;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static RewardDefinition FromJson(JsonData root) {
		int gold = 0;
		if(root.Keys.Contains("gold")) {
			gold = (int)root["gold"];
		}

		return new RewardDefinition(gold);
	}

	#endregion

}
