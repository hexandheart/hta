﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class CampaignDefinition {

	public string Name { get; private set; }

	public int StartingGold { get; private set; }

	/************************************************************************/
	#region Construction

	private CampaignDefinition(string name, int startingGold) {
		Name = name;
		StartingGold = startingGold;
	}

	#endregion

	/************************************************************************/
	#region Serialization
		
	public static readonly string ElementName = "campaign";

	public static CampaignDefinition FromJson(JsonData root) {
		string name = "Unnamed Campaign";
		if(root.Keys.Contains("name")) {
			name = (string)root["name"];
		}

		int startingGold = 0;
		if(root.Keys.Contains("startingGold")) {
			startingGold = (int)root["startingGold"];
		}

		return new CampaignDefinition(name, startingGold);
	}

	#endregion

	/************************************************************************/
	#region Privates

	#endregion

}
