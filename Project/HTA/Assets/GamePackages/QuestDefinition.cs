﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class QuestDefinition {

	public string ID { get; private set; }
	public string Name { get; private set; }
	public string Info { get; private set; }
	public int HeroSlots { get; private set; }

	public RewardDefinition Reward { get; private set; }
	public ActivityTemplateDefinition Template { get; private set; }

	/************************************************************************/
	#region Construction

	private QuestDefinition(string id, string name, string info, int heroSlots, RewardDefinition reward, ActivityTemplateDefinition template) {
		ID = id;
		Name = name;
		Info = info;
		HeroSlots = heroSlots;
		Reward = reward;
		Template = template;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static readonly string GroupName = "quests";

	public static QuestDefinition FromJson(JsonData root) {
		string id = (string)root["id"];

		string name = id;
		if(root.Keys.Contains("name")) {
			name = (string)root["name"];
		}

		string info = id;
		if(root.Keys.Contains("info")) {
			info = (string)root["info"];
		}

		int heroSlots = 1;
		if(root.Keys.Contains("heroSlots")) {
			heroSlots = (int)root["heroSlots"];
		}

		RewardDefinition reward = RewardDefinition.Default;
		if(root.Keys.Contains("reward")) {
			reward = RewardDefinition.FromJson(root["reward"]);
		}

		ActivityTemplateDefinition template = ActivityTemplateDefinition.Default;
		if(root.Keys.Contains(ActivityTemplateDefinition.ElementName)) {
			template = ActivityTemplateDefinition.FromJson(root[ActivityTemplateDefinition.ElementName]);
		}

		return new QuestDefinition(id, name, info, heroSlots, reward, template);
	}

	#endregion

}
