﻿using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XPTable {

	public string ID { get; private set; }

	/// <summary>
	/// Returns the total amount of XP required to be exactly the level specified.
	/// ie GetTotalXPForLevel(1) is 0, GetTotalXPForLevel(2) is the first value in the XP table.
	/// GetTotalXPForLevel(N) is the last value in the XP table.
	/// </summary>
	/// <param name="level"></param>
	/// <returns></returns>
	public int GetTotalXPForLevel(int level) {
		if((level-1) <= 0) {
			return 0;
		}
		if(level > GetMaxLevel()) {
			return GetTotalXPForLevel(GetMaxLevel());
		}

		return xpTable[(level-1) - 1];
	}

	public int GetXPForLevel(int level) {
		if(level > GetMaxLevel()) {
			return GetXPForLevel(GetMaxLevel());
		}

		return GetTotalXPForLevel(level) - GetTotalXPForLevel(level - 1);
	}

	public int GetLevelForTotalXP(int xp) {
		for(int i = 0; i < xpTable.Length; ++i) {
			if(xp < xpTable[i]) {
				return i + 1;
			}
		}
		return GetMaxLevel();
	}

	public int GetMaxLevel() {
		return xpTable.Length + 1;
	}

	/************************************************************************/
	#region Construction

	private XPTable(string id, int[] xpTable) {
		ID = id;
		this.xpTable = xpTable;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static XPTable FromJson(JsonData root) {
		string id = (string)root["id"];

		int[] xpTable = new int[0];
		if(root.Keys.Contains("xp")) {
			JsonData element = root["xp"];
			xpTable = new int[element.Count];
			for(int i = 0; i < xpTable.Length; ++i) {
				xpTable[i] = (int)element[i];
			}
		}

		return new XPTable(id, xpTable);
	}

	#endregion

	/************************************************************************/
	#region Privates

	private int[] xpTable;

	#endregion

}
