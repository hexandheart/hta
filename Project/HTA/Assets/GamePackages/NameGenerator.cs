﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class NameGenerator {

	public static readonly string ElementName = "nameGenerator";

	public string Generate(string group) {
		string start = "";
		string mid = "";
		string end = "";

		if(starts.ContainsKey(group)) {
			start = starts[group][Random.Range(0, starts[group].Count)];
		}
		if(mids.ContainsKey(group)) {
			mid = mids[group][Random.Range(0, mids[group].Count)];
		}
		if(ends.ContainsKey(group)) {
			end = ends[group][Random.Range(0, ends[group].Count)];
		}

		return start + mid + end;
	}

	/************************************************************************/
	#region Serialization

	public void AddFromJson(JsonData root) {
		for(int groupIndex = 0; groupIndex < root.Count; ++groupIndex) {
			JsonData groupData = root[groupIndex];
			string group = (string)groupData["group"];
			JsonData data;

			if(!starts.ContainsKey(group)) {
				starts[group] = new List<string>();
			}
			data = groupData["starts"];
			for(int i = 0; i < data.Count; ++i) {
				starts[group].Add((string)data[i]);
			}

			if(!mids.ContainsKey(group)) {
				mids[group] = new List<string>();
			}
			data = groupData["mids"];
			for(int i = 0; i < data.Count; ++i) {
				mids[group].Add((string)data[i]);
			}

			if(!ends.ContainsKey(group)) {
				ends[group] = new List<string>();
			}
			data = groupData["ends"];
			for(int i = 0; i < data.Count; ++i) {
				ends[group].Add((string)data[i]);
			}
		}
	}

	#endregion

	/************************************************************************/
	#region Privates

	private Dictionary<string, List<string>> starts = new Dictionary<string, List<string>>();
	private Dictionary<string, List<string>> mids = new Dictionary<string, List<string>>();
	private Dictionary<string, List<string>> ends = new Dictionary<string, List<string>>();

	#endregion

}
