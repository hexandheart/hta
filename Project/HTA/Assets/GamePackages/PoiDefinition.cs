﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public abstract class PoiDefinition {

	public string ID { get; private set; }
	public string Name { get; private set; }
	public PoiType Type { get; private set; }

	/************************************************************************/
	#region Construction

	protected PoiDefinition(string id, string name, PoiType type) {
		ID = id;
		Name = name;
		Type = type;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static PoiDefinition FromJson(JsonData root) {
		string id = (string)root["id"];

		string name = id;
		if(root.Keys.Contains("name")) {
			name = (string)root["name"];
		}

		PoiType type = PoiType.Questing;
		if(root.Keys.Contains("type")) {
			type = (PoiType)System.Enum.Parse(typeof(PoiType), (string)root["type"], true);
		}

		switch(type) {
		case PoiType.Questing:
			return new PoiDefinitions.QuestingDefinition(id, name, type, root);
		case PoiType.Recruitment:
			return new PoiDefinitions.RecruitmentDefinition(id, name, type, root);
		case PoiType.Recovery:
			return new PoiDefinitions.RecoveryDefinition(id, name, type, root);
		}

		return null;
	}

	#endregion

}
