﻿using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillDefinition {

	public string ID { get; private set; }
	public string Name { get; private set; }

	public string XPTableID { get; private set; }
	public SkillRating BaselingRating { get; private set; }
	public SkillSpell[] Spells { get; private set; }

	/************************************************************************/
	#region Construction

	private SkillDefinition(
			string id, string name,
			string xpTableId,
			SkillRating baselineRating,
			SkillSpell[] spells) {
		ID = id;
		Name = name;
		XPTableID = xpTableId;
		BaselingRating = baselineRating;
		Spells = spells;
	}

	#endregion

	/************************************************************************/
	#region Construction

	public static SkillDefinition FromJson(JsonData root) {
		string id = (string)root["id"];

		string name = id;
		if(root.Keys.Contains("name")) {
			name = (string)root["name"];
		}

		string xpTableId = "";
		if(root.Keys.Contains("xpTable")) {
			xpTableId = (string)root["xpTable"];
		}

		SkillRating baselineRating = SkillRating.E;
		if(root.Keys.Contains("baselineRating")) {
			baselineRating = (SkillRating)System.Enum.Parse(typeof(SkillRating), (string)root["baselineRating"], true);
		}

		SkillSpell[] spells;
		if(root.Keys.Contains("spells")) {
			JsonData element = root["spells"];
			spells = new SkillSpell[element.Count];
			for(int i = 0; i < spells.Length; ++i) {
				spells[i] = SkillSpell.FromJson(element[i]);
			}
		}
		else {
			spells = new SkillSpell[0];
		}

		return new SkillDefinition(id, name, xpTableId, baselineRating, spells);
	}

	#endregion

	/************************************************************************/
	#region SkillSpell

	public struct SkillSpell {

		public int Level { get; private set; }
		public string SpellID { get; private set; }

		public static SkillSpell FromJson(JsonData root) {
			return new SkillSpell() {
				Level = (int)root["level"],
				SpellID = (string)root["id"],
			};
		}

	}

	#endregion

}
