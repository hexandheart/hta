﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class ActivityEventDefinition {

	public ActivityEventType Type { get; private set; }
	public int EventIndex { get; private set; }
	public int TriggerChance { get; private set; }
	public int TriggerDuration { get; private set; }
	public int MinProgress { get; private set; }
	public int MaxProgress { get; private set; }
	public JsonData Data { get; private set; }

	/************************************************************************/
	#region Construction

	private ActivityEventDefinition(
			ActivityEventType type,
			int eventIndex,
			int triggerChance, int triggerDuration,
			int minProgress, int maxProgress,
			JsonData data) {
		Type = type;
		EventIndex = eventIndex;
		TriggerChance = triggerChance;
		TriggerDuration = triggerDuration;
		MinProgress = minProgress;
		MaxProgress = maxProgress;
		Data = data;
	}

	public ActivityEventDefinition(ActivityEventDefinition o) {
		Type = o.Type;
		EventIndex = o.EventIndex;
		TriggerChance = o.TriggerChance;
		TriggerDuration = o.TriggerDuration;
		MinProgress = o.MinProgress;
		MaxProgress = o.MaxProgress;
		if(o.Data != null) {
			Data = JsonMapper.ToObject(o.Data.ToJson());
		}
		else {
			Data = null;
		}
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static ActivityEventDefinition FromJson(JsonData root, int index) {
		ActivityEventType type = ActivityEventType.RandomRoll;
		if(root.Keys.Contains("type")) {
			type = (ActivityEventType)System.Enum.Parse(typeof(ActivityEventType), (string)root["type"], true);
		}

		int eventIndex = index;

		int triggerChance = 100;
		if(root.Keys.Contains("triggerChance")) {
			triggerChance = (int)root["triggerChance"];
		}

		int triggerDuration = 100;
		if(root.Keys.Contains("triggerDuration")) {
			triggerDuration = (int)root["triggerDuration"];
		}

		int minProgress = 0;
		if(root.Keys.Contains("minProgress")) {
			minProgress = (int)root["minProgress"];
		}

		int maxProgress = 0;
		if(root.Keys.Contains("maxProgress")) {
			maxProgress = (int)root["maxProgress"];
		}

		JsonData data = null;
		if(root.Keys.Contains("data")) {
			data = root["data"];
		}

		return new ActivityEventDefinition(type, eventIndex, triggerChance, triggerDuration, minProgress, maxProgress, data);
	}

	public JsonData ToJson() {
		return new JsonData() {
			["type"] = Type.ToString(),
			["triggerChance"] = TriggerChance,
			["triggerDuration"] = TriggerDuration,
			["minProgress"] = MinProgress,
			["maxProgress"] = MaxProgress,
			["data"] = Data,
		};
	}

	#endregion

}
