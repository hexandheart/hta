﻿using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EffectType {
	Harm,
	Help,
}

public class SpellDefinition {

	public delegate void SpellHandler(SpellDefinition spell);

	public string ID { get; private set; }
	public string Name { get; private set; }
	public string Description { get; private set; }
	public EffectType EffectType { get; private set; }

	public int Potency { get; private set; }
	public int Spread { get; private set; }

	public bool CanTargetSelf { get; private set; }
	public bool CanTargetAlly { get; private set; }
	public bool CanTargetEnemy { get; private set; }

	public string GetDescription() {
		int spread = Spread * Potency / 100;
		return string.Format(Description, string.Format("{0}-{1}", Mathf.Abs(Potency - spread), Mathf.Abs(Potency + spread)));
	}

	/************************************************************************/
	#region Construction

	private SpellDefinition(
			string id, string name, string description,
			EffectType effectType,
			int potency, int spread,
			bool canTargetSelf, bool canTargetAlly, bool canTargetEnemy) {
		ID = id;
		Name = name;
		Description = description;
		EffectType = effectType;

		Potency = potency;
		Spread = spread;

		CanTargetAlly = canTargetAlly;
		CanTargetEnemy = canTargetEnemy;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static SpellDefinition FromJson(JsonData root) {
		string id = (string)root["id"];

		string name = id;
		if(root.Keys.Contains("name")) {
			name = (string)root["name"];
		}

		string description = "";
		if(root.Keys.Contains("description")) {
			description = (string)root["description"];
		}

		EffectType effectType = EffectType.Harm;
		if(root.Keys.Contains("type")) {
			effectType = (EffectType)System.Enum.Parse(typeof(EffectType), (string)root["type"], true);
		}

		int potency = 0;
		if(root.Keys.Contains("potency")) {
			potency = (int)root["potency"];
		}

		int spread = 20;
		if(root.Keys.Contains("spread")) {
			spread = (int)root["spread"];
		}

		bool canTargetSelf = false;
		bool canTargetAlly = false;
		bool canTargetEnemy = false;
		if(root.Keys.Contains("target")) {
			JsonData element = root["target"];
			if(element.Keys.Contains("self")) {
				canTargetSelf = (bool)element["self"];
			}
			if(element.Keys.Contains("ally")) {
				canTargetAlly = (bool)element["ally"];
			}
			if(element.Keys.Contains("enemy")) {
				canTargetEnemy = (bool)element["enemy"];
			}
		}

		return new SpellDefinition(id, name, description, effectType, potency, spread, canTargetSelf, canTargetAlly, canTargetEnemy);
	}

	#endregion

}
