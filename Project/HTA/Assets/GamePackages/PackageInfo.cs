﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class PackageInfo {
	
	public string ID { get; private set; }
	public string Name { get; private set; }

	public List<string> Include { get; private set; }
	
	/************************************************************************/
	#region Construction

	private PackageInfo(string id, string name, List<string> include) {
		ID = id;
		Name = name;
		Include = include;
	}

	#endregion

	/************************************************************************/
	#region Serialization

	public static readonly string ElementName = "packageInfo";

	public static PackageInfo FromJson(JsonData root) {
		string id = (string)root["id"];

		string name = id;
		if(root.Keys.Contains("name")) {
			name = (string)root["name"];
		}

		List<string> include = new List<string>();
		if(root.Keys.Contains("include")) {
			JsonData element = root["include"];
			for(int i = 0; i < element.Count; ++i) {
				include.Add((string)element[i]);
			}
		}

		return new PackageInfo(id, name, include);
	}

	#endregion

	/************************************************************************/
	#region Privates

	#endregion

}
