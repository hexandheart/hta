﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HeroGenerator {

	public static Hero Generate(WorldState world, HeroOwner owner) {
		string id = System.Guid.NewGuid().ToString();
		string name = world.NameGenerator.Generate("core");
		var mantles = world.GetMantlesByType(MantleType.Race);
		MantleDefinition race = mantles[Random.Range(0, mantles.Count)];
		mantles = world.GetMantlesByType(MantleType.Job);
		MantleDefinition job = mantles[Random.Range(0, mantles.Count)];

		// Generate starting skills.
		List<Skill> skills = new List<Skill>();
		Skill.GenerateHireSkills(race, job, skills, world);

		return new Hero(id, name, owner, race, job, skills);
	}

	public static Hero Generate(WorldState world, HeroOwner owner, MantleDefinition race, MantleDefinition job, string nameOverload = null) {
		string id = System.Guid.NewGuid().ToString();
		string name = nameOverload;
		if(string.IsNullOrEmpty(name)) {
			world.NameGenerator.Generate("core");
		}

		// Generate starting skills.
		List<Skill> skills = new List<Skill>();
		Skill.GenerateHireSkills(race, job, skills, world);

		return new Hero(id, name, owner, race, job, skills);
	}

}
